<div class="container elenco-clienti">

	<div class="row">
		<div class="col-md-6 col-sm-6 col-xs-8">
			<h1>
				<i class="fa fa-briefcase"></i>Servizi
			</h1>
		</div>
		<div class="col-md-6 col-sm-6 col-xs-4">
			<div class="title-right">
				<a id="addRow" class="btn btn-lg btn-success">
					<i class="fa fa-plus"></i>
					<span>
						Nuovo servizio
					</span>
				</a>
			</div>
		</div>
	</div>
</div>
<form method="post" action="class/class-servizi.php">

	<section>
	
		<div class="container">
			
			<div class="row">
				<div class="col-md-9 col-sm-8 col-xs-7">
					<label>
						Servizio
					</label>
				</div>
				<div class="col-md-3 col-sm-4 col-xs-5">
					<label>
						Prezzo
					</label>
				</div>
				
				
				
				
				<?php
				
					$query = "SELECT servizio,prezzo FROM servizi ORDER BY id ASC";
					// esecuzione della query
					$result = $conn->query($query);
					// conteggio dei record restituiti dalla query
					if($result->num_rows >0){
						// generazione di un array numerico
						while($row = $result->fetch_array(MYSQLI_NUM)){
						
						
							echo '
							
								<div class="serviceRow">
									<div class="col-md-9 col-sm-8 col-xs-6">
										<div class="form-group">
											<input type="text" class="form-control" id="servizio" value="'.$row[0].'" name="service-name[]">
										</div>
									</div>
									
									<div class="col-md-3 col-sm-4 col-xs-6">
										<div class="form-group itemPrice">
											<input type="text" class="form-control" id="prezzo" value="'.$row[1].'" name="service-price[]">
											<i class="fa fa-close sbutton del-service"></i>
										</div>
									</div>
								</div>
							
							';

						}
					}else{
						
						echo '
						
						
							<div class="serviceRow">
								<div class="col-md-9 col-sm-8 col-xs-6">
									<div class="form-group">
										<input type="text" class="form-control" id="servizio" name="service-name[]">
									</div>
								</div>
								
								<div class="col-md-3 col-sm-4 col-xs-6">
									<div class="form-group itemPrice">
										<input type="text" class="form-control" id="prezzo" name="service-price[]">
										<i class="fa fa-close sbutton del-service"></i>
									</div>
								</div>
							</div>
						
						';
					}
				
				?>

				<div class="endItem"></div>
				
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<button type="submit" class="btn-lg btn-success">
						SALVA
					</button>
					<a href="#" onclick="location.reload(true);" class="annulla-form">
						Annulla
					</a>
				</div>
			</div><!--/.row-->
			
		</div>
	</section>

</form>



