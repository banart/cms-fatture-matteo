<?php

	error_reporting(E_ALL);
	ini_set("display_errors",1);

	$query = " SELECT * FROM fatture ORDER BY numero ASC ";
	$result = $conn->query($query);
	
	$anni = array();
	while($row = $result->fetch_array()){
		$anni[] = $row['anno'];
	}
	
	$anni = array_unique($anni);
	$anni_implode = implode(',', $anni);
	
	$query = " SELECT * FROM fatture WHERE anno in (" . $anni_implode . ") ORDER BY numero ASC ";
	$result = $conn->query($query);
	while($row = $result->fetch_array()){
		$progressivi[$row['anno']][] = $row['numero'];
	}
	
	
	// Cerco i progressivi mancanti
	if(isset($progressivi[$anno][0])){
	
		$ultimo_progressivo = max($progressivi[$anno]);
		$range_numeri = range(1, $ultimo_progressivo);
		$progressivi_mancanti = array_diff($range_numeri, $progressivi[$anno]);
		
		$separetor = "";
		$elenco_numeri_mancanti = "";
		foreach($progressivi_mancanti as $i => $num){
			$elenco_numeri_mancanti = $elenco_numeri_mancanti . $separetor . "$num";
			$separetor = " - ";
		}
		
	}else{
		$ultimo_progressivo = 0;
		$progressivi_mancanti = 0;
		$elenco_numeri_mancanti = "";
	}
	
	$listafatture = array();
	foreach($progressivi as $i => $pr){
		foreach($pr as $a => $nm){
			$listafatture[] =  $i . "_" . $nm;
		}
	}

	if(!empty($elenco_numeri_mancanti)){
		$mancano_numeri = 1;
	}else{
		$mancano_numeri = 0;
	}
	
	
	if(isset($_POST['mod_fattura'])){
		$prog = $_POST['numero'];
		$data = $_POST['data'];
	}else{
		$prog = $ultimo_progressivo+1;
		$data = $date;
	}
	
	
	$query = "SELECT * FROM fisco WHERE role = 'admin'";
	$result = $conn->query($query);
	$fisco = $result->fetch_array();
	
	if(isset($_POST['mod_fattura'])){
		$query = "SELECT * FROM fatture WHERE numero = '" . $_POST['numero'] . "'";
		$result = $conn->query($query);
		$fattura = $result->fetch_array();
	}
	
	
	// echo "<pre>" . print_r($listafatture,1) . "</pre>";
	
?>

<div class="nuova-fattura">

	<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h1>
				<?php
					if(isset($_POST['mod_fattura'])){
						echo "<i class=\"fa fa-pencil\"></i>Modifica fattura";
					}else{
						echo "<i class=\"fa fa-file-text\"></i>Nuova fattura";
					}
				?>
			</h1>
		</div>
	</div>
</div>
	
	
	<form method="post" action="index.php?page=riepilogo-fattura" id="form_fattura">
	<!--<form method="post" action="class/class-nuova-fattura.php" id="form_fattura">-->

	<section>

		<div class="container">
		
			<div class="row">
		
			<div class="col-md-1 col-sm-2 col-xs-4">
				<div class="form-group">
					<label for="progressivo">Numero</label>
					<input type="text" class="form-control numero-progressivo" id="progressivo" name="numero" value="<?php echo $prog ?>">
				</div>
			</div>
			
			<div class="col-md-2 col-sm-2 col-xs-8">
				<div class="form-group">
					<label for="data">Data</label>
					<input type="date" class="form-control date" id="data" value="<?php echo $data ?>" name="data">
				</div>
			</div>
			
			<div class="col-md-9 col-sm-8 col-xs-12">
				<div class="form-group add-client">
					<label>
						Cliente
					</label>
					<div class="styled-select">
						<select class="form-control" name="cliente" id="cliente">
							<option></option>
							
							<?php
						
								$query = " SELECT id,ragione_sociale FROM clienti ORDER BY id ASC ";
								$result = $conn->query($query);
								
								if($result->num_rows >0){
									while($row = $result->fetch_array()){
										
										if(isset($_POST['cliente'])){
											if($row["id"] == $_POST['cliente']){
												$selected = " selected";
											}else{
												$selected = "";
											}
										}
									
										echo '<option' . $selected . ' value="'.$row["id"].'">'.$row["ragione_sociale"].'</option>';
									}
								}
							?>
							
						</select>
					</div>
					<!-- <a href="#"><i class="fa fa-plus"></i></a> -->
				</div>
			</div>
			<div class="col-md-9 col-md-offset-3 col-sm-8 col-sm-offset-4 col-xs-12">
				<a class="lnk" href="?page=nuovo-cliente">Registra un nuovo cliente</a>
			</div>	
		</div>
	
		</div>
	
	</section>
	
	<section>
	
		<div class="container">
			
			<div class="row">
			
				<div class="col-md-12">
					<h3 class="title-section">
						Voci fattura
					</h3>
				</div>
			
				<div class="col-md-9 col-sm-8 col-xs-7">
				<label>
					Servizio
				</label>
			</div>
				<div class="col-md-3 col-sm-4 col-xs-5">
				<label>
					Prezzo
				</label>
			</div>
				<div>
				
					<?php if(isset($_POST['mod_fattura'])){
						foreach($_POST['servizi'] as $i => $servizio){
					?>
					
						<div class="itemRow">
							<div class="col-md-9 col-sm-8 col-xs-6">
								<div class="form-group">
									<input type="text" class="form-control itemSelect" name="servizi[]" id="item" value="<?php echo $servizio ?>">
								</div>
							</div>
							
							<div class="col-md-3 col-sm-4 col-xs-6">
								<div class="form-group itemPrice">
									<input type="text" class="form-control" name="prezzi[]" value="<?php echo $_POST['prezzi'][$i] ?>">
									<i class="fa fa-close sbutton"></i>
									<i id="addRow" class="fa fa-plus sbutton"></i>
								</div>
							</div>
						</div>
					
					<?php
							}
						}else{
					?>
					
						<div class="itemRow">
							<div class="col-md-9 col-sm-8 col-xs-6">
								<div class="form-group">
									<input type="text" class="form-control itemSelect" name="servizi[]" id="item">
								</div>
							</div>
							
							<div class="col-md-3 col-sm-4 col-xs-6">
								<div class="form-group itemPrice">
									<input type="text" class="form-control" name="prezzi[]">
									<i class="fa fa-close sbutton"></i>
									<i id="addRow" class="fa fa-plus sbutton"></i>
								</div>
							</div>
						</div>
						
					<?php } ?>
					
				</div>
				
				<div class="endItem"></div>
				
			</div>
		
		</div>
	</section>
	
	<?php
		if(isset($_POST['mod_fattura'])){
			if(isset($_POST['bollo'])){
				$bollo_checked = " checked";
				$bollo_value = $_POST['idbollo'];
			}else{
				$bollo_checked = "";
				$bollo_value = "";
			}
		}else{
			$bollo_checked = "";
			$bollo_value = "";
		}
	?>
	
	<?php if($fisco['bollo'] == 1){ ?>
		<section>
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h3 class="title-section">
							Bollo
						</h3>
					</div>
					<div class="col-md-2 col-sm-3">
						<div class="form-group">
							<label>Assolto all'origine</label>
							<input type="checkbox" name="bollo" id="bollo-origine" <?php echo $bollo_checked ?>>
							<label for="bollo-origine"></label>
						</div>
					</div>
					<div class="col-md-10 col-sm-9">
						<div class="form-group showbollo">
							<label for="idbollo">ID Bollo</label>
							<input type="text" class="form-control" id="idbollo" name="idbollo" value="<?php echo $bollo_value ?>">
						</div>
					</div>
				</div>
			</div>
		</section>
	<?php } ?>
	
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<button type="submit" class="btn-lg btn-success" id="form_submit">
					RIEPILOGO FATTURA
				</button>
			</div>
		</div>
	</div>

</form>
	
</div>


<script>

	
	
	// inizio ASSEGNAZIONE VARIABILI //
	
	// Booleano per controllo se sono presenti numeri mancanti
	var mancano_numeri = <?php echo $mancano_numeri ?>;
	
	// Numeri non presenti tra i progressivi, formattati come stringa
	var elenco_numeri = "<?php echo $elenco_numeri_mancanti ?>";
	
	// Numero progressivo successivo al numero più alto tra i progressivi
	var prossimo_progressivo = <?php echo $ultimo_progressivo ?>+1;
	
	// Numero attualmente impostato nel campo progressivo
	var progressivo = $(".numero-progressivo").val();
	
	// Assegno valore a data corrente e data inserita
	
	<?php if(isset($_POST['mod_fattura'])){ ?>
		var date = "<?php echo $data  ?>";
		var date_val = $(".date").val();
		var year_arr = $('.date').val().split("/");
		var year = year_arr[2];
	<?php }else{ ?>
		var date = "<?php echo $date ?>";
		var date_val = $(".date").val();
		var year_arr = $('.date').val().split("/");
		var year = year_arr[2];
	<?php } ?>
	
	// Assegno valore al cliente
	var cliente = $("#cliente").val();
	
	// Assegno valore primo servizio
	var servizio = $(".itemRow:first-child input#item").val();
	
	// Inizializzo la variabili di testo 
	var alert_assegnato = "";
	var alert_progressivo = "";
	var alert_mancanti = "";
	var alert_date = "";
	var alert_cliente = "";
	var alert_servizio = "";


	// Converto gli array PHP in array JS
	var progressivi_assegnati = <?php echo json_encode($progressivi) ?>;
	var progressivi_mancanti = <?php echo json_encode($progressivi_mancanti) ?>;
	var listafatture = <?php echo json_encode($listafatture) ?>;

	// fine ASSEGNAZIONE VARIABILI //
	
	var bloccante = 0;
	
	
	
	
	// Controllo se sono presenti numeri mancanti e valorizzo una stringa
	if(mancano_numeri == 1){
		alert_mancanti = "All\'elenco fatture mancano i seguenti progressivi:<br />" + elenco_numeri + "<br /><br />";
	}
	
	// Controllo che il valore del campo progressivo sia quello corretto, cioè il successivo al numero più alto
	if (progressivo == prossimo_progressivo){
		alert_progressivo = "";
	}else{
		if(progressivo == ""){
			alert_progressivo = "Non hai inserito il numero della fattura";
			bloccante = 1;
		}else{
			alert_progressivo = "Il numero di fattura selezionato non è progressivo. Il numero corretto è <b>" + prossimo_progressivo + "</b>.<br /><br />";
		}
	}
	
	// Controllo che la data corrente sia uguale alla data inserita in caso di modifica
	if(date_val == date){
		alert_date = "";
	}else{
		if(date_val == ""){
			alert_date = "Non hai inserito la data<br /><br />";
			bloccante = 1;
		}else{
			alert_date = "La data inserita non è la data di oggi.<br /><br />";
		}
	}
	
	// Controllo che il campo "Clientr" sia valorizzato
	if(cliente == ""){
		alert_cliente = "Non hai selezionato il cliente<br /><br />";
		bloccante = 1;
	}else{
		alert_cliente = "";
	}
	
	// Controllo che almeno un campo "Servizio" sia valorizzato
	if(servizio == ""){
		alert_servizio = "Non hai inserito nessun servizio<br /><br />";
		bloccante = 1;
	}else{
		alert_servizio = "";
	}
	
	if(bloccante == 1){
		var alert_confirm = "";
	}else{
		var alert_confirm = "<strong>Si è sicuri di voler procedere?</strong>";
	}
	
	// Compongo la stringa di alert
	var alert  = alert_progressivo + alert_mancanti + alert_date + alert_cliente + alert_servizio + alert_confirm;
	
	// Controllo se il nuovo valore non sia tra i progressivi già assegnati
	$.each(listafatture, function(index,value){
		if(value == progressivo){
			alert = "Il numero progressivo per la fattura è già in uso.<br /><br />" + alert_date + alert_cliente + alert_servizio + alert_confirm;
			bloccante = 1;
		}
	});
	
	// Controllo che il numero valorizzato sia uno dei numeri mancanti. In questo caso cambio il messaggio di alert
	$.each(progressivi_mancanti, function(index, value){
		if(progressivo == value){
			alert = "Il numero fattura impostato (<b>" + progressivo + "</b>), è tra i numeri progressivi non ancora assegnati.<br />Considera però che il progressivo successivo è <b>" + prossimo_progressivo + "</b>.<br /><br />" + alert_date + alert_cliente + alert_servizio + alert_confirm;
		}
	})
	
	
	
	
	
	
	// Funzione in caso di cambiamento del valore del campo progressivo
	$(function() {
		$("#form_fattura").on('change',function(event){

			// Riassegno alla variabile 'progressivo' il nuovo valore del campo progressivo
		    progressivo = $(".numero-progressivo").val();
		    
		    bloccante = 0;
		    
		    // Riassegno alla variabile 'date_val' il nuovo valore del campo data
		    date_val = $(".date").val();
		    year_arr = $('.date').val().split("/");
		    year = year_arr[2];
		    
		    // Assegno valore al cliente
		    var cliente = $("#cliente").val();
		    
		    // Assegno nuovo valore primo servizio
		    var servizio = $(".itemRow:first-child input#item").val();
		    
		    // Controllo che il valore del campo progressivo sia quello corretto, cioè il successivo al numero più alto
		    if (progressivo == prossimo_progressivo){
				alert_progressivo = "";
			}else{
				if(progressivo == ""){
					alert_progressivo = "Non hai inserito il numero della fattura<br /><br />";
					bloccante = 1;
				}else{
					alert_progressivo = "Il numero di fattura selezionato non è progressivo. Il numero corretto è <b>" + prossimo_progressivo + "</b>.<br /><br />";
				}
			}
			
			// Controllo che la data corrente sia uguale alla data inserita in caso di modifica
			if(date_val == date){
				alert_date = "";
			}else{
				if(date_val == ""){
					alert_date = "Non hai inserito la data";
					bloccante = 1;
				}else{
					alert_date = "La data inserita non è la data di oggi.<br /><br />";
				}
			}

			
			if(cliente == ""){
				alert_cliente = "Non hai selezionato il cliente<br /><br />";
				bloccante = 1;
			}else{
				alert_cliente = "";
			}
				
			if(servizio == ""){
				alert_servizio = "Non hai inserito nessun servizio<br /><br />";
				bloccante = 1;
			}else{
				alert_servizio = "";
			}
			
			if(bloccante == 1){
				var alert_confirm = "";
			}else{
				var alert_confirm = "<strong>Si è sicuri di voler procedere?</strong>";
			}
			
			// Compongo la stringa di alert
			alert  = alert_progressivo + alert_mancanti + alert_assegnato + alert_date + alert_cliente + alert_servizio + alert_confirm;
			
			// Controllo se il nuovo valore non sia tra i progressivi già assegnati
			$.each(progressivi_assegnati, function(index,value){
				if(value == progressivo){
					alert = "Il numero progressivo per la fat-tura è già in uso.<br /><br />" + alert_date + alert_cliente + alert_servizio + alert_confirm;
					bloccante = 1;
				}
			});

			// Controllo che il numero valorizzato sia uno dei numeri mancanti. In questo caso cambio il messaggio di alert
			$.each(progressivi_mancanti, function(index, value){
				if(progressivo == value){
					alert = "Il numero fattura impostato (<b>" + progressivo + "</b>), è tra i numeri progressivi non ancora assegnati.<br />Considera però che il progressivo successivo è <b>" + prossimo_progressivo + "</b>.<br /><br />" + alert_date + alert_cliente + alert_servizio + alert_confirm;
				}
			});

	    });
	    
    });





    // ALERT DI CONFERMA
	$('#form_submit').on('click', function (event) {
		
		if(mancano_numeri == 0 && alert_progressivo == "" && alert_cliente == ""){
			return;
		}

		event.preventDefault();

		if(bloccante == 1){
			$.alert({
		        theme: 'supervan',
		        title: '<i class="fa fa-warning"></i><br />Attenzione!',
		        content: alert,
		        confirmButton: 'Ho capito',
		        confirm: function(){
		        	// alert('Confirmed!');
		        }
		    });
		}else{
			$.confirm({
		        theme: 'supervan',
		        title: '<i class="fa fa-warning"></i><br />Attenzione!',
		        content: alert,
		        confirm: function(){
		        	$('#form_fattura').submit();
		        },
		        cancel: function(){
		            // alert('Canceled!')
		        }
		    });
		}

	    
	});
	
</script>