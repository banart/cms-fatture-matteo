<?php
	if (isset($_GET['y'])){
		$year = $_GET['y'];
	}else{
		$year = $anno;
	}
	
	
	$query = "SELECT * FROM fatture";
	$result = $conn->query($query);
	$anni = array();
	while($row = $result->fetch_array()){
		$anni[] = $row['anno'];
	}
	$anni = array_unique($anni);
	
	
	
	$query = "SELECT * FROM fatture WHERE anno = '" . $year . "' ORDER BY numero DESC";
	$result = $conn->query($query);
	
	//echo "<pre>" . print_r($anni,1) . "</pre>";
?>

<div class="container elenco-clienti">

	<div class="row">
		<div class="col-md-6 col-sm-6">
			<h1>
				<i class="fa fa-file-text"></i>Fatture <span class="year-selector"><?php echo $year ?></span>
			</h1>
		</div>
		<div class="col-md-6 col-sm-6 hidden-xs">
			<div class="title-right">
				<a href="?page=nuova-fattura" class="btn btn-lg btn-success">
					<i class="fa fa-plus"></i> Nuova fattura
				</a>
			</div>
		</div>
	</div>
	
</div>

<section>
	<div class="container">
	
		<form method="post" action="class/class-zip.php">
		
			<input type="hidden" name="anno" value="<?php echo $year ?>" />
	
			<?php
				if($result->num_rows >0){
			?>
	
			<div class="row">
			
				<div class="col-md-12">
					<div class="linea-fattura prima-linea-fattura">
						<div class="col-fattura">
							<input type="checkbox" name="#" id="selectall" />
							<label for="selectall"></label>
						</div>
						<div class="col-fattura">
							<button type="submit" class="button-link">
								<i class="fa fa-cloud-download"></i> Scarica selezionati
							</button>
						</div>
					</div>
				</div>
			
			</div>
			
			<?php } ?>
		
			<div class="row">
	
			<div class="col-md-12">
			
				<?php

					if($result->num_rows >0){

						while($row = $result->fetch_array()){
						
							$nome_fattura = $row['anno'] . "_" . $row['numero'];
				?>

					<div class="linea-fattura">
						<a href="pdf/<?php echo $nome_fattura . ".pdf"  ?>" target="_blank">
						<!--
						<a href="index.php?page=riepilogo-fattura&from=<?php echo $_GET['page'] ?>&id=<?php echo $row['id'] ?>">
						-->
							<div class="col-fattura">
								<input type="checkbox" name="zipfatture[<?php echo $nome_fattura ?>]" value="<?php echo $nome_fattura ?>" id="<?php echo $row['id'] ?>" class="selectable" />
								<label for="<?php echo $row['id'] ?>"></label>
							</div>
							<div class="col-fattura id-fattura">
								<?php echo $row['numero'] ?>
							</div>
							<div class="col-fattura data-fattura">
								<?php
									$date_array = explode("-",$row['data']);
									$date = $date_array[2]."/".$date_array[1];
									echo $date;
								?>
							</div>
							<div class="col-fattura destinatario-fatura">
								<?php
									$query2 = "SELECT * FROM clienti WHERE id =" . $row['cliente'];
									$result2 = $conn->query($query2);
									$cliente = $result2->fetch_array();
									echo $cliente['ragione_sociale'] . "<span>(€ " . $row['totale'] . ")</span>";
								?>
							</div>
						</a>
						<div class="box-modifica-fattura">
							<a href="class/class-elimina-fattura.php?id=<?php echo $row['id'] ?>" class="del-fattura">
								<i class="fa fa-close sbutton"></i>
							</a>
							<!--
							<a href="?page=modifica-fattura&id=">
								<i class="fa fa-pencil sbutton"></i>
							</a>
							-->			
						</div>
					</div>
				
				<?php }
						}else{
							echo "<p>";
							echo "Non è presente ancora nessuna fattura in elenco. Per crearne una <a href=\"?page=nuova-fattura\"><b>clicca qui</b></a>.";
							echo "</p>";
						}
				?>
				
				
	
			</div>
			
			
		</div>
		
		</form>
	</div>
</section>

</div>

<div class="lista-anni">
	<div class="chiudi-lista-anni">
		<i class="fa fa-close"></i>
	</div>
	<h3>
		Anno fatturazione
	</h3>
	<div>
		<?php
			foreach($anni as $i => $anno){
				echo "<a hreF='?page=elenco-fatture&y=" . $anno . "'>";
				echo $anno;
				echo "</a>";
			}
		?>
	</div>
</div>