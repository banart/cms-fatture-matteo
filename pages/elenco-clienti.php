<div class="container elenco-clienti">

	<div class="row">
		<div class="col-md-6 col-sm-6">
			<h1>
				<i class="fa fa-users"></i>Elenco clienti
			</h1>
		</div>
		<div class="col-md-6 col-sm-6 hidden-xs">
			<div class="title-right">
				<a href="?page=nuovo-cliente" class="btn btn-lg btn-success">
					<i class="fa fa-plus"></i> Nuovo cliente
				</a>
			</div>
		</div>
	</div>
</div>

<section>
	<div class="container">

		<div class="row">
	
		<div class="col-md-12">

			<?php

				$query = " SELECT id,ragione_sociale FROM clienti ORDER BY id ASC ";

				$result = $conn->query($query);

				if($result->num_rows >0){
					while($row = $result->fetch_array()){
					
						echo '
						
							<div class="linea-cliente">
								<a href="?page=cliente&id='.$row['id'].'">' . 
									$row["ragione_sociale"]
								 . '</a>
								<div class="box-modifica-cliente">
									<a href="class/class-elimina-cliente.php?id='.$row['id'].'" class="del-client">
										<i class="fa fa-close sbutton"></i>
									</a>
									<a href="?page=modifica-cliente&id=' . $row["id"] . '">
										<i class="fa fa-pencil sbutton"></i>
									</a>				
								</div>
							</div>
						
						';

					}
				}else{
					echo "<p>";
					echo "Non è presente ancora nessun cliente nella lista. Per aggiungerne un clente, <a href=\"?page=nuovo-cliente\"><b>clicca qui</b></a>";
					echo "</p>";
				}
			
			?>
			
		
		</div>
		
	</div>
	
	</div>
</section>


</div>