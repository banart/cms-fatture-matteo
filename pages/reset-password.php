<?php
	if(isset($_SESSION['error_reset_password_email']) && $_SESSION['error_reset_password_email'] !== 0){
	
		$err_email = "field-error";
		
		if($_SESSION['error_reset_password_email'] == 1){
			$err_email_mess = '<div class="mess-error">L\'indirizzo email è errato o non esistente!</div>';
		}elseif($_SESSION['error_reset_password_email'] == "null"){
			$err_email_mess = '<div class="mess-error">Non hai inserito l\'indirizzo email!</div>';
		}else{
			$err_email_mess = "";
		}
		
	}else{
		$err_email = "";
	}
	
	if(isset($_SESSION['reset_password_email'])){
		$value_email = "value=\"" . $_SESSION['login_email'] . "\"";
	}else{
		$value_email = "";
	}
?>

<div class="container reset-password">
	<div class="row">
			
		<?php
			if(isset($_POST['send_reset_password'])){
		?>
		
			<div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
			
				<h2>
					<i class="fa fa-lock"></i> Reset password
				</h2>
				<p>
					Inserisci l'indirizzo email del tuo account, al quale invieremo il link per resettare la tua password.
				</p>
	
				<form class="form-horizontal" action="class/class-reset-password.php" method="post">
	
					<input type="hidden" name="rstatus" value="1" />
					
					<div class="form-group">
						<div class="col-md-12">
							<input type="email" name="email" class="form-control <?php echo $err_email ?>" placeholder="Email" <?php echo $value_email ?>>
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-12">
							<button type="submit" class="btn btn-success">Invia</button>
						</div>
					</div>
				
				</form>
			
			</div>
		
		
		<?php
			}else{
				if(isset($_GET['key'])){
			?>
		
		
				<div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
				
					<h2>
						<i class="fa fa-lock"></i> Reset password
					</h2>
					<p>
						Inserisci la nuova password due volte.
					</p>
		
					<form class="form-horizontal" action="class/class-reset-password.php" method="post">
					
						<input type="hidden" name="reset_password_status" value="2" />
					
						<div class="form-group">
							<div class="col-md-12">
								<input type="password" name="password_1" class="form-control <?php echo $err_password ?>" placeholder="Password">
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-12">
								<input type="password" name="password_2" class="form-control <?php echo $err_password ?>" placeholder="Ripeti password">
							</div>
						</div>
	
						<div class="form-group">
							<div class="col-md-12">
								<button type="submit" class="btn btn-success">Salva</button>
							</div>
						</div>
					
					</form>
				
				</div>
		
		
			<?php
				}else{
					echo "Non dovresti essere qui";
				}
			}
		?>
			
	</div>
</div>