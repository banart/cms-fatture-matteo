
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h1>
				<i class="fa fa-user"></i>Nuovo cliente
			</h1>
		</div>
	</div>
</div>

<form method="post" action="class/class-nuovo-cliente.php">

	<section>
	
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-4">
					<div class="form-group">
						<label for="ragione-sociale">Ragione Sociale</label>
						<input type="text" class="form-control" id="ragione-sociale" name="ragione-sociale">
					</div>
				</div>
				
				<div class="col-md-4 col-sm-4">
					<div class="form-group">
						<label for="partita-iva">Partita IVA</label>
						<input type="text" class="form-control" id="partita-iva" name="partita-iva">
					</div>
				</div>
				
				<div class="col-md-4 col-sm-4">
					<div class="form-group">
						<label for="codice-fiscale">Codice Fiscale</label>
						<input type="text" class="form-control" id="codice-fiscale" name="codice-fiscale">
					</div>
				</div>
			
			</div><!--/.row-->
		</div><!--/.container-->
		
	</section>

	
	<section>
	
		<div class="container">
		
			<div class="row">
			
				<div class="col-md-12">
					<h3 class="title-section">
						Indirizzo
					</h3>
				</div>
			
				<div class="col-md-4 col-sm-10 col-xs-9">
					<div class="form-group">
						<label for="via">Via/Vico/Strada/...</label>
						<input type="text" class="form-control" id="via" name="via">
					</div>
				</div>
				
				<div class="col-md-1 col-sm-2 col-xs-3">
					<div class="form-group">
						<label for="civico">Civico</label>
						<input type="text" class="form-control" id="civico" name="civico">
					</div>
				</div>
				
				<div class="col-md-2 col-sm-3 col-xs-4">
					<div class="form-group">
						<label for="cap">CAP</label>
						<input type="text" class="form-control" id="cap" name="cap">
					</div>
				</div>
				
				<div class="col-md-2 col-sm-4 col-xs-8">
					<div class="form-group">
						<label for="citta">Città</label>
						<input type="text" class="form-control" id="citta" name="citta">
					</div>
				</div>
				
				<div class="col-md-3 col-sm-5 col-xs-12">
					<div class="form-group add-client">
						<label for="provincia">
							Provincia
						</label>
						<div class="styled-select">
							<select class="form-control" id="provincia" name="provincia">
								<option></option>
								<option value="ag">Agrigento</option>
								<option value="al">Alessandria</option>
								<option value="an">Ancona</option>
								<option value="ao">Aosta</option>
								<option value="ar">Arezzo</option>
								<option value="ap">Ascoli Piceno</option>
								<option value="at">Asti</option>
								<option value="av">Avellino</option>
								<option value="ba">Bari</option>
								<option value="bt">Barletta-Andria-Trani</option>
								<option value="bl">Belluno</option>
								<option value="bn">Benevento</option>
								<option value="bg">Bergamo</option>
								<option value="bi">Biella</option>
								<option value="bo">Bologna</option>
								<option value="bz">Bolzano</option>
								<option value="bs">Brescia</option>
								<option value="br">Brindisi</option>
								<option value="ca">Cagliari</option>
								<option value="cl">Caltanissetta</option>
								<option value="cb">Campobasso</option>
								<option value="ci">Carbonia-iglesias</option>
								<option value="ce">Caserta</option>
								<option value="ct">Catania</option>
								<option value="cz">Catanzaro</option>
								<option value="ch">Chieti</option>
								<option value="co">Como</option>
								<option value="cs">Cosenza</option>
								<option value="cr">Cremona</option>
								<option value="kr">Crotone</option>
								<option value="cn">Cuneo</option>
								<option value="en">Enna</option>
								<option value="fm">Fermo</option>
								<option value="fe">Ferrara</option>
								<option value="fi">Firenze</option>
								<option value="fg">Foggia</option>
								<option value="fc">Forl&igrave;-Cesena</option>
								<option value="fr">Frosinone</option>
								<option value="ge">Genova</option>
								<option value="go">Gorizia</option>
								<option value="gr">Grosseto</option>
								<option value="im">Imperia</option>
								<option value="is">Isernia</option>
								<option value="sp">La spezia</option>
								<option value="aq">L'aquila</option>
								<option value="lt">Latina</option>
								<option value="le">Lecce</option>
								<option value="lc">Lecco</option>
								<option value="li">Livorno</option>
								<option value="lo">Lodi</option>
								<option value="lu">Lucca</option>
								<option value="mc">Macerata</option>
								<option value="mn">Mantova</option>
								<option value="ms">Massa-Carrara</option>
								<option value="mt">Matera</option>
								<option value="vs">Medio Campidano</option>
								<option value="me">Messina</option>
								<option value="mi">Milano</option>
								<option value="mo">Modena</option>
								<option value="mb">Monza e della Brianza</option>
								<option value="na">Napoli</option>
								<option value="no">Novara</option>
								<option value="nu">Nuoro</option>
								<option value="og">Ogliastra</option>
								<option value="ot">Olbia-Tempio</option>
								<option value="or">Oristano</option>
								<option value="pd">Padova</option>
								<option value="pa">Palermo</option>
								<option value="pr">Parma</option>
								<option value="pv">Pavia</option>
								<option value="pg">Perugia</option>
								<option value="pu">Pesaro e Urbino</option>
								<option value="pe">Pescara</option>
								<option value="pc">Piacenza</option>
								<option value="pi">Pisa</option>
								<option value="pt">Pistoia</option>
								<option value="pn">Pordenone</option>
								<option value="pz">Potenza</option>
								<option value="po">Prato</option>
								<option value="rg">Ragusa</option>
								<option value="ra">Ravenna</option>
								<option value="rc">Reggio di Calabria</option>
								<option value="re">Reggio nell'Emilia</option>
								<option value="ri">Rieti</option>
								<option value="rn">Rimini</option>
								<option value="rm">Roma</option>
								<option value="ro">Rovigo</option>
								<option value="sa">Salerno</option>
								<option value="ss">Sassari</option>
								<option value="sv">Savona</option>
								<option value="si">Siena</option>
								<option value="sr">Siracusa</option>
								<option value="so">Sondrio</option>
								<option value="ta">Taranto</option>
								<option value="te">Teramo</option>
								<option value="tr">Terni</option>
								<option value="to">Torino</option>
								<option value="tp">Trapani</option>
								<option value="tn">Trento</option>
								<option value="tv">Treviso</option>
								<option value="ts">Trieste</option>
								<option value="ud">Udine</option>
								<option value="va">Varese</option>
								<option value="ve">Venezia</option>
								<option value="vb">Verbano-Cusio-Ossola</option>
								<option value="vc">Vercelli</option>
								<option value="vr">Verona</option>
								<option value="vv">Vibo valentia</option>
								<option value="vi">Vicenza</option>
								<option value="vt">Viterbo</option>
							</select>
						</div>
						<!-- <a href="#"><i class="fa fa-plus"></i></a> -->
					</div>
				</div>
			
			</div>
		
		</div>

	</section>
	
	
	<section>
		<div class="container">
			<div class="row">
				
				<div class="col-md-12">
					<h3 class="title-section">
						Contatti
					</h3>
				</div>
				
				<div class="col-md-4 col-sm-4 col-xs-6">
					<div class="form-group">
						<label for="sito-web">Sito web</label>
						<input type="text" class="form-control" id="sito-web" name="sito-web" value="http://">
					</div>
				</div>
				
				<div class="col-md-4 col-sm-4 col-xs-6">
					<div class="form-group">
						<label for="email">E-mail</label>
						<input type="text" class="form-control" id="email" name="email">
					</div>
				</div>
				
				<div class="col-md-4 col-sm-4 col-xs-6">
					<div class="form-group">
						<label for="telefono-fisso">Telefono fisso</label>
						<input type="text" class="form-control" id="telefono-fisso" name="telefono-fisso">
					</div>
				</div>
				
				<div class="col-md-4 col-sm-4 col-xs-6">
					<div class="form-group">
						<label for="telefono-mobile">Telefono mobile</label>
						<input type="text" class="form-control" id="telefono-mobile" name="telefono-mobile">
					</div>
				</div>
				
				<div class="col-md-4 col-sm-4 col-xs-6">
					<div class="form-group">
						<label for="fax">FAX</label>
						<input type="text" class="form-control" id="fax" name="fax">
					</div>
				</div>
				
				<div class="col-md-4 col-sm-4 col-xs-6">
					<div class="form-group">
						<label for="skype">Skype</label>
						<input type="text" class="form-control" id="skype" name="skype">
					</div>
				</div>
				
			</div>
		</div>
	</section>
	
	<section>
	
		<div class="container">
			<div class="row">
			
				<div class="col-md-12">
					<h3 class="title-section">
						Referente
					</h3>
				</div>
			
				<div class="col-md-6 col-sm-6 col-xs-6">
					<div class="form-group">
						<label for="nome-referente">Nome</label>
						<input type="text" class="form-control" id="nome-referente" name="nome-referente">
					</div>
				</div>
				
				<div class="col-md-6 col-sm-6 col-xs-6">
					<div class="form-group">
						<label for="ruolo-referente">Ruolo</label>
						<input type="text" class="form-control" id="ruolo-referente" name="ruolo-referente">
					</div>
				</div>
				
				<div class="col-md-4 col-sm-4 col-xs-6">
					<div class="form-group">
						<label for="email-referente">E-mail</label>
						<input type="text" class="form-control" id="email-referente" name="email-referente">
					</div>
				</div>
				
				<div class="col-md-4 col-sm-4 col-xs-6">
					<div class="form-group">
						<label for="telefono-referente">Telefono</label>
						<input type="text" class="form-control" id="telefono-referente" name="telefono-referente">
					</div>
				</div>
				
				<div class="col-md-4 col-sm-4 col-xs-6">
					<div class="form-group">
						<label for="skype-referente">Skype</label>
						<input type="text" class="form-control" id="skype-referente" name="skype-referente">
					</div>
				</div>
			
			</div><!--/.row-->
		</div><!--/.container-->
		
	</section>
	
	
	<div class="container">

		<div class="row">
			<div class="col-md-12">
				<button type="submit" class="btn-lg btn-success">
					AGGIUNGI CLIENTE
				</button>
			</div>
		</div>

	</div>
	


</form>