<?php
	$query = "SELECT * FROM clienti WHERE id=" . $_GET['id'];
	$result = $conn->query($query);
	$row = $result->fetch_array();
	
	$query = "SELECT * FROM province ORDER BY nome ASC";
	$result = $conn->query($query);
?>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h1>
				<i class="fa fa-pencil"></i>Modifica cliente
			</h1>
		</div>
	</div>
</div>

<form method="post" action="class/class-modifica-cliente.php?id=<?php echo $_GET['id'] ?>">

	<section>
	
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-4">
					<div class="form-group">
						<label for="ragione-sociale">Ragione Sociale</label>
						<input type="text" class="form-control" id="ragione-sociale" name="ragione-sociale" value="<?php echo $row['ragione_sociale'] ?>">
					</div>
				</div>
				
				<div class="col-md-4 col-sm-4">
					<div class="form-group">
						<label for="partita-iva">Partita IVA</label>
						<input type="text" class="form-control" id="partita-iva" name="partita-iva" value="<?php echo $row['partita_iva'] ?>">
					</div>
				</div>
				
				<div class="col-md-4 col-sm-4">
					<div class="form-group">
						<label for="codice-fiscale">Codice Fiscale</label>
						<input type="text" class="form-control" id="codice-fiscale" name="codice-fiscale" value="<?php echo $row['codice_fiscale'] ?>">
					</div>
				</div>
			
			</div><!--/.row-->
		</div><!--/.container-->
		
	</section>

	
	<section>
	
		<div class="container">
		
			<div class="row">
			
				<div class="col-md-12">
					<h3 class="title-section">
						Indirizzo
					</h3>
				</div>
			
				<div class="col-md-4 col-sm-10 col-xs-9">
					<div class="form-group">
						<label for="via">Via/Vico/Strada/...</label>
						<input type="text" class="form-control" id="via" name="via" value="<?php echo $row['via'] ?>">
					</div>
				</div>
				
				<div class="col-md-1 col-sm-2 col-xs-3">
					<div class="form-group">
						<label for="civico">Civico</label>
						<input type="text" class="form-control" id="civico" name="civico" value="<?php echo $row['civico'] ?>">
					</div>
				</div>
				
				<div class="col-md-2 col-sm-3 col-xs-4">
					<div class="form-group">
						<label for="cap">CAP</label>
						<input type="text" class="form-control" id="cap" name="cap" value="<?php echo $row['cap'] ?>">
					</div>
				</div>
				
				<div class="col-md-2 col-sm-4 col-xs-8">
					<div class="form-group">
						<label for="citta">Città</label>
						<input type="text" class="form-control" id="citta" name="citta" value="<?php echo $row['citta'] ?>">
					</div>
				</div>
				
				<div class="col-md-3 col-sm-5 col-xs-12">
					<div class="form-group add-client">
						<label for="provincia">
							Provincia
						</label>
						<div class="styled-select">
							<select class="form-control" id="provincia" name="provincia">
								<option></option>
								<?php
									while($prv = $result->fetch_array()){
										if ($prv['sigla'] == $row['provincia']){
											$sel = ' selected="selected"';
										}else{
											$sel = '';
										}
										echo '<option value="'.$prv['sigla'].'"'.$sel.'>'.$prv['nome'].'</option>';
									}
								?>
							</select>
						</div>
						<!-- <a href="#"><i class="fa fa-plus"></i></a> -->
					</div>
				</div>
			
			</div>
		
		</div>

	</section>
	
	
	<section>
		<div class="container">
			<div class="row">
				
				<div class="col-md-12">
					<h3 class="title-section">
						Contatti
					</h3>
				</div>
				
				<div class="col-md-4 col-sm-4 col-xs-6">
					<div class="form-group">
						<label for="sito-web">Sito web</label>
						<input type="text" class="form-control" id="sito-web" name="sito-web" value="<?php echo $row['sito_web'] ?>">
					</div>
				</div>
				
				<div class="col-md-4 col-sm-4 col-xs-6">
					<div class="form-group">
						<label for="email">E-mail</label>
						<input type="text" class="form-control" id="email" name="email" value="<?php echo $row['email'] ?>">
					</div>
				</div>
				
				<div class="col-md-4 col-sm-4 col-xs-6">
					<div class="form-group">
						<label for="telefono-fisso">Telefono fisso</label>
						<input type="text" class="form-control" id="telefono-fisso" name="telefono-fisso" value="<?php echo $row['telefono_fisso'] ?>">
					</div>
				</div>
				
				<div class="col-md-4 col-sm-4 col-xs-6">
					<div class="form-group">
						<label for="telefono-mobile">Telefono mobile</label>
						<input type="text" class="form-control" id="telefono-mobile" name="telefono-mobile" value="<?php echo $row['telefono_mobile'] ?>">
					</div>
				</div>
				
				<div class="col-md-4 col-sm-4 col-xs-6">
					<div class="form-group">
						<label for="fax">FAX</label>
						<input type="text" class="form-control" id="fax" name="fax" value="<?php echo $row['fax'] ?>">
					</div>
				</div>
				
				<div class="col-md-4 col-sm-4 col-xs-6">
					<div class="form-group">
						<label for="skype">Skype</label>
						<input type="text" class="form-control" id="skype" name="skype" value="<?php echo $row['skype'] ?>">
					</div>
				</div>
				
			</div>
		</div>
	</section>
	
	<section>
	
		<div class="container">
			<div class="row">
			
				<div class="col-md-12">
					<h3 class="title-section">
						Referente
					</h3>
				</div>
			
				<div class="col-md-6 col-sm-6 col-xs-6">
					<div class="form-group">
						<label for="nome-referente">Nome</label>
						<input type="text" class="form-control" id="nome-referente" name="nome-referente" value="<?php echo $row['nome_referente'] ?>">
					</div>
				</div>
				
				<div class="col-md-6 col-sm-6 col-xs-6">
					<div class="form-group">
						<label for="ruolo-referente">Ruolo</label>
						<input type="text" class="form-control" id="ruolo-referente" name="ruolo-referente" value="<?php echo $row['ruolo_referente'] ?>">
					</div>
				</div>
				
				<div class="col-md-4 col-sm-4 col-xs-6">
					<div class="form-group">
						<label for="email-referente">E-mail</label>
						<input type="text" class="form-control" id="email-referente" name="email-referente" value="<?php echo $row['email_referente'] ?>">
					</div>
				</div>
				
				<div class="col-md-4 col-sm-4 col-xs-6">
					<div class="form-group">
						<label for="telefono-referente">Telefono</label>
						<input type="text" class="form-control" id="telefono-referente" name="telefono-referente" value="<?php echo $row['telefono_referente'] ?>">
					</div>
				</div>
				
				<div class="col-md-4 col-sm-4 col-xs-6">
					<div class="form-group">
						<label for="skype-referente">Skype</label>
						<input type="text" class="form-control" id="skype-referente" name="skype-referente" value="<?php echo $row['skype_referente'] ?>">
					</div>
				</div>
			
			</div><!--/.row-->
		</div><!--/.container-->
		
	</section>
	
	
	<div class="container">

		<div class="row">
			<div class="col-md-12">
				<button type="submit" class="btn-lg btn-success">
					SALVA
				</button>
			</div>
		</div>

	</div>
	


</form>