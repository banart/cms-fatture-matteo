
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h1>
				<i class="fa fa-file-text"></i>Modifica fattura
			</h1>
		</div>
	</div>
</div>


<form method="post" action="#">

	<section>

		<div class="container">
		
			<div class="row">
		
			<div class="col-md-1 col-sm-2 col-xs-4">
				<div class="form-group">
					<label for="progressivo">Numero</label>
					<input type="text" class="form-control" id="progressivo" value="3">
				</div>
			</div>
			
			<div class="col-md-2 col-sm-2 col-xs-8">
				<div class="form-group">
					<label for="data">Data</label>
					<input type="date" class="form-control" id="data" placeholder="<?php echo $date ?>">
				</div>
			</div>
			
			<div class="col-md-9 col-sm-8 col-xs-12">
				<div class="form-group add-client">
					<label>
						Cliente
					</label>
					<div class="styled-select">
						<select class="form-control" name="cliente" id="cliente">
							<option value="Cliente">Cliente</option>
							<option value="Cliente">Cliente</option>
							<option value="Cliente">Cliente</option>
							<option value="Cliente">Cliente</option>
							<option value="Cliente">Cliente</option>
						</select>
					</div>
					<!-- <a href="#"><i class="fa fa-plus"></i></a> -->
				</div>
			</div>
			
			<div class="col-md-9 col-md-offset-3 col-sm-8 col-sm-offset-4 col-xs-12">
				<a class="lnk" href="?page=nuovo-cliente">Registra un nuovo cliente</a>
			</div>
			
		</div>
	
		</div>
	
	</section>
	
	<section>
	
		<div class="container">
			
			<div class="row">
				<div class="col-md-9 col-sm-8 col-xs-7">
				<label>
					Servizio
				</label>
			</div>
				<div class="col-md-3 col-sm-4 col-xs-5">
				<label>
					Prezzo
				</label>
			</div>
				<div>
					<div class="itemRow">
						<div class="col-md-9 col-sm-8 col-xs-6">
							<div class="form-group">
								<select class="form-control" name="item" id="item">
									<option value="item">Voce fattura</option>
									<option value="item">Voce fattura</option>
									<option value="item">Voce fattura</option>
									<option value="item">Voce fattura</option>
									<option value="item">Voce fattura</option>
								</select>
							</div>
						</div>
						
						<div class="col-md-3 col-sm-4 col-xs-6">
							<div class="form-group itemPrice">
								<input type="text" class="form-control" id="prezzo" placeholder="">
								<i class="fa fa-close sbutton"></i>
								<i id="addRow" class="fa fa-plus sbutton"></i>
							</div>
						</div>
					</div>
				</div>
				
				<div class="endItem"></div>
				
			</div>
		
			
			<div class="row">
				<div class="col-md-12 buttonsItems">
					<a class="lnk" href="?page=nuovo-servizio">
						Registra un nuovo servizio
					</a>
				</div>
			</div>
			
		</div>
	</section>
	
	<section>
	
		<div class="container">
			
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label for="idbollo">ID Bollo</label>
						<input type="text" class="form-control" id="idbollo" placeholder="">
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<button type="submit" class="btn-lg btn-success">
						SALVA MODIFICHE
					</button>
				</div>
			</div>
		
		</div>
	
	</section>

</form>