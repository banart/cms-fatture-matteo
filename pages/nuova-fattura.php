<?php

	error_reporting(E_ALL);
	ini_set("display_errors",1);

	$query = " SELECT * FROM fatture ORDER BY numero ASC ";
	$result = $conn->query($query);
	
	$anni = array();
	while($row = $result->fetch_array()){
		$anni[] = $row['anno'];
	}
	
	if(empty($anni)){
		$anni[] = $anno;
	}
	
	$anni = array_unique($anni);
	$anni_implode = implode(',', $anni);
	
	$query = " SELECT * FROM fatture WHERE anno in (" . $anni_implode . ") ORDER BY numero ASC ";
	$result = $conn->query($query);
	while($row = $result->fetch_array()){
		$progressivi[$row['anno']][] = $row['numero'];
	}
	if(!isset($progressivi)){
		$progressivi = 0;
	}
		
	
	// Cerco i progressivi mancanti
	// DA CONTROLLARE L'UTILITA' E LA NECESSITA' DI AVERE QUESTO BLOCCO
	if(isset($progressivi[$anno][0])){
	
		$ultimo_progressivo = max($progressivi[$anno]);
		$range_numeri = range(1, $ultimo_progressivo);
		$progressivi_mancanti = array_diff($range_numeri, $progressivi[$anno]);
		
		$separetor = "";
		$elenco_numeri_mancanti = "";
		foreach($progressivi_mancanti as $i => $num){
			$elenco_numeri_mancanti = $elenco_numeri_mancanti . $separetor . "$num";
			$separetor = " - ";
		}
		
	}else{
		$ultimo_progressivo = 0;
		$progressivi_mancanti = 0;
		$elenco_numeri_mancanti = "";
	}

	if(!empty($elenco_numeri_mancanti)){
		$mancano_numeri = 1;
	}else{
		$mancano_numeri = 0;
	}
	
	
	$listafatture = array();
	if(!empty($progressivi)){
		foreach($progressivi as $i => $pr){
			foreach($pr as $a => $nm){
				$listafatture[] =  $i . "_" . $nm;
			}
		}
	}
	

	
	if(isset($_POST['mod_fattura'])){
		$prog = $_POST['numero'];
		$data = $_POST['data'];
	}else{
		$prog = $ultimo_progressivo+1;
		$data = $date;
	}
	
	
	$query = "SELECT * FROM fisco WHERE role = 'admin'";
	$result = $conn->query($query);
	$fisco = $result->fetch_array();
	
	if(isset($_POST['mod_fattura'])){
		$query = "SELECT * FROM fatture WHERE numero = '" . $_POST['numero'] . "'";
		$result = $conn->query($query);
		$fattura = $result->fetch_array();
	}
	
	
	// echo "<pre>" . print_r($elenco_numeri_mancanti,1) . "</pre>";
	
?>

<div class="nuova-fattura">

	<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h1>
				<?php
					if(isset($_POST['mod_fattura'])){
						echo "<i class=\"fa fa-pencil\"></i>Modifica fattura";
					}else{
						echo "<i class=\"fa fa-file-text\"></i>Nuova fattura";
					}
				?>
			</h1>
		</div>
	</div>
</div>
	
	
	<form method="post" action="index.php?page=riepilogo-fattura" id="form_fattura">
	<!--<form method="post" action="class/class-nuova-fattura.php" id="form_fattura">-->

	<section>

		<div class="container">
		
			<div class="row">
		
			<div class="col-md-1 col-sm-2 col-xs-4">
				<div class="form-group">
					<label for="progressivo">Numero</label>
					<input type="text" class="form-control numero-progressivo" id="progressivo" name="numero" value="<?php echo $prog ?>">
				</div>
			</div>
			
			<div class="col-md-2 col-sm-2 col-xs-8">
				<div class="form-group">
					<label for="data">Data</label>
					<input type="text" class="form-control date" id="data" value="<?php echo $data ?>" name="data">
				</div>
			</div>
			
			<div class="col-md-9 col-sm-8 col-xs-12">
				<div class="form-group add-client">
					<label>
						Cliente
					</label>
					<div class="styled-select">
						<select class="form-control" name="cliente" id="cliente">
							<option></option>
							
							<?php
						
								$query = " SELECT id,ragione_sociale FROM clienti ORDER BY id ASC ";
								$result = $conn->query($query);
								
								if($result->num_rows >0){
									while($row = $result->fetch_array()){
										
										if(isset($_POST['cliente'])){
											if($row["id"] == $_POST['cliente']){
												$selected = " selected";
											}else{
												$selected = "";
											}
										}
									
										echo '<option' . $selected . ' value="'.$row["id"].'">'.$row["ragione_sociale"].'</option>';
									}
								}
							?>
							
						</select>
					</div>
					<!-- <a href="#"><i class="fa fa-plus"></i></a> -->
				</div>
			</div>
			<div class="col-md-9 col-md-offset-3 col-sm-8 col-sm-offset-4 col-xs-12">
				<a class="lnk" href="?page=nuovo-cliente">Registra un nuovo cliente</a>
			</div>	
		</div>
	
		</div>
	
	</section>
	
	<section>
	
		<div class="container">
			
			<div class="row">
			
				<div class="col-md-12">
					<h3 class="title-section">
						Voci fattura
					</h3>
				</div>
			
				<div class="col-md-9 col-sm-8 col-xs-7">
				<label>
					Servizio
				</label>
			</div>
				<div class="col-md-3 col-sm-4 col-xs-5">
				<label>
					Prezzo
				</label>
			</div>
				<div>
				
					<?php if(isset($_POST['mod_fattura'])){
						foreach($_POST['servizi'] as $i => $servizio){
					?>
					
						<div class="itemRow">
							<div class="col-md-9 col-sm-8 col-xs-6">
								<div class="form-group">
									<input type="text" class="form-control itemSelect" name="servizi[]" id="item" value="<?php echo $servizio ?>">
								</div>
							</div>
							
							<div class="col-md-3 col-sm-4 col-xs-6">
								<div class="form-group itemPrice">
									<input type="text" class="form-control" name="prezzi[]" value="<?php echo $_POST['prezzi'][$i] ?>">
									<i class="fa fa-close sbutton"></i>
									<i id="addRow" class="fa fa-plus sbutton"></i>
								</div>
							</div>
						</div>
					
					<?php
							}
						}else{
					?>
					
						<div class="itemRow">
							<div class="col-md-9 col-sm-8 col-xs-6">
								<div class="form-group">
									<input type="text" class="form-control itemSelect" name="servizi[]" id="item">
								</div>
							</div>
							
							<div class="col-md-3 col-sm-4 col-xs-6">
								<div class="form-group itemPrice">
									<input type="text" class="form-control" name="prezzi[]">
									<i class="fa fa-close sbutton"></i>
									<i id="addRow" class="fa fa-plus sbutton"></i>
								</div>
							</div>
						</div>
						
					<?php } ?>
					
				</div>
				
				<div class="endItem"></div>
				
			</div>
		
		</div>
	</section>
	
	<?php
		if(isset($_POST['mod_fattura'])){
			if(isset($_POST['bollo'])){
				$bollo_checked = " checked";
				$bollo_value = $_POST['idbollo'];
			}else{
				$bollo_checked = "";
				$bollo_value = "";
			}
		}else{
			$bollo_checked = "";
			$bollo_value = "";
		}
	?>
	
	<?php if($fisco['bollo'] == 1){ ?>
		<section>
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h3 class="title-section">
							Bollo
						</h3>
					</div>
					<div class="col-md-2 col-sm-3">
						<div class="form-group">
							<label>Assolto all'origine</label>
							<input type="checkbox" name="bollo" id="bollo-origine" <?php echo $bollo_checked ?>>
							<label for="bollo-origine"></label>
						</div>
					</div>
					<div class="col-md-10 col-sm-9">
						<div class="form-group showbollo">
							<label for="idbollo">ID Bollo</label>
							<input type="text" class="form-control" id="idbollo" name="idbollo" value="<?php echo $bollo_value ?>">
						</div>
					</div>
				</div>
			</div>
		</section>
	<?php } ?>
	
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<button type="submit" class="btn-lg btn-success" id="form_submit">
					RIEPILOGO FATTURA
				</button>
			</div>
		</div>
	</div>

</form>
	
</div>


<script>

	/* ------------------ *|
	 RECUPERO I DATI DA PHP
	|* ------------------ */

	// Booleano per controllo se sono presenti numeri mancanti
	var mancano_numeri = <?php echo $mancano_numeri ?>;
	
	// Numeri non presenti tra i progressivi, formattati come stringa
	var elenco_numeri = "<?php echo $elenco_numeri_mancanti ?>";
	
	// Numero progressivo successivo al numero più alto tra i progressivi
	var prossimo_progressivo = <?php echo $ultimo_progressivo ?>+1;
	
	// Assegno valore a data corrente e data inserita
	<?php if(isset($_POST['mod_fattura'])){ ?>
		var date = "<?php echo $data  ?>";
	<?php }else{ ?>
		var date = "<?php echo $date ?>";
	<?php } ?>
	
	// Converto gli array PHP in array JS
	var progressivi_assegnati = <?php echo json_encode($progressivi) ?>;
	var progressivi_mancanti = <?php echo json_encode($progressivi_mancanti) ?>;
	var listafatture = <?php echo json_encode($listafatture) ?>;

</script>
<script src="js/nuova-fattura.js"></script>

