<?php

	// echo "<pre>" . print_r($_POST,1) . "</pre>";
	
	if(isset($_GET['from']) && $_GET['from'] == 'elenco-fatture'){
	
		$query = "SELECT * FROM fatture WHERE id = '" . $_GET['id'] . "'";
		$result = $conn->query($query);
		$fattura = $result->fetch_array();
		
		$id_cliente = $fattura['cliente'];
		$bollo = $fattura['bollo'];
		$id_bollo = $fattura['id_bollo'];
		
		$modmode = 1;
		
	}else{
		$id_cliente = $_POST['cliente'];
		if(isset($_POST['bollo'])){
			$bollo_check = $_POST['bollo'];
		}
		
		if(isset($_POST['idbollo'])){
			$id_bollo = $_POST['idbollo'];
		}
		
		$modmode = 0;
	}
	
	if($id_cliente != ""){
		$query = "SELECT * FROM clienti WHERE id=" . $id_cliente;
		$result = $conn->query($query);
		$cliente = $result->fetch_array();
	}
	
	
	if(isset($_GET['from']) && $_GET['from'] == 'elenco-fatture'){
		$elenco_servizi = explode("|", $fattura['servizi']);
		$elenco_prezzi = explode("|", $fattura['prezzi']);
	}else{
		$elenco_servizi = $_POST['servizi'];
		$elenco_prezzi = $_POST['prezzi'];
	}
	
	$query = "SELECT * FROM anagrafica WHERE role = 'admin'";
	$result = $conn->query($query);
	$anagrafica = $result->fetch_array();

	$query = "SELECT * FROM layout WHERE role = 'admin'";
	$result = $conn->query($query);
	$layout = $result->fetch_array();

	$query = "SELECT * FROM fisco WHERE role = 'admin'";
	$result = $conn->query($query);
	$fisco = $result->fetch_array();
	
	


	/**********************************/
	/* FORMATTAZIONE ANAGRAFICA ADMIN */
	/**********************************/
	
	if($anagrafica['ragione_sociale'] != ""){
		$ragione_sociale_admin = "<li class='evidenza'>".$anagrafica['ragione_sociale']."</li>";
	}else{
		$ragione_sociale_admin = "";
	}
	
	if($anagrafica['via'] != "" || $anagrafica['citta'] != "" ){
		
		$open_li = "<li class='margine-separatore'>";
		$close_li = "</li>";
		
		if($anagrafica['via'] != ""){
			$via_admin = $anagrafica['via'];
		}else{
			$via_admin = "";
		}
		
		if($anagrafica['civico'] != 0){
			$civico_admin = ", ".$anagrafica['civico'];
		}else{
			$civico_admin = "";
		}
		
		if($anagrafica['cap'] != 0){
			$cap_admin = $anagrafica['cap'] . " - ";
		}else{
			$cap_admin = "";
		}
		
		if($anagrafica['citta'] != ""){
			$citta_admin = $anagrafica['citta'];
		}else{
			$citta_admin = "";
		}
		
		if($anagrafica['provincia'] != ""){
			$provincia_admin = "(<span class='maiuscolo'>".$anagrafica['provincia']."</span>)";
		}else{
			$provincia_admin = "";
		}
		
		$indirizzo_admin =
			$open_li .
			$via_admin . $civico_admin . "<br />" .
			$cap_admin . $citta_admin . " " . $provincia_admin .
			$close_li;
	}else{
		$indirizzo_admin = "";
	}
	
	if($anagrafica['partita_iva'] != 0){
		$partita_iva_admin = "<li>P.I.: ".$anagrafica['partita_iva']."</li>";
	}else{
		$partita_iva_admin = "";
	}
	
	if($anagrafica['codice_fiscale'] != ""){
		$codice_fiscale_admin = "<li>C.F.: ".$anagrafica['codice_fiscale']."</li>";
	}else{
		$codice_fiscale_admin = "";
	}
	
	if($anagrafica['telefono_fisso'] != ""){
		$telefono_fisso_admin = "<li>Tel: ".$anagrafica['telefono_fisso']."</li>";
	}else{
		$telefono_fisso_admin = "";
	}
	
	if($anagrafica['telefono_mobile'] != ""){
		$telefono_mobile_admin = "<li>Cell: ".$anagrafica['telefono_mobile']."</li>";
	}else{
		$telefono_mobile_admin = "";
	}
	
	if($anagrafica['sito_web'] != ""){
		$sito_admin = "<li>Sito web: ".$anagrafica['sito_web']."</li>";
	}else{
		$sito_admin = "";
	}
	
	if($anagrafica['email'] != ""){
		$email_admin = "<li>Email: ".$anagrafica['email']."</li>";
	}else{
		$email_admin = "";
	}
	
	if($anagrafica['skype'] != ""){
		$skype_admin = "<li>Skype: ".$anagrafica['skype']."</li>";
	}else{
		$skype_admin = "";
	}

	$is_admin = "<ul>".
		$ragione_sociale_admin .
		$indirizzo_admin .
		$partita_iva_admin .
		$codice_fiscale_admin .
		$telefono_fisso_admin .
		$telefono_mobile_admin .
		$sito_admin .
		$email_admin .
		$skype_admin . 
	"</ul>";
	
	/**************fine****************/
	/* FORMATTAZIONE ANAGRAFICA ADMIN */
	/**************fine****************/
	



	
	/************************************/
	/* FORMATTAZIONE ANAGRAFICA CLIENTE */
	/************************************/
	
	if(isset($_GET['from']) && $_GET['from'] == 'elenco-fatture'){
	
		$numero = $fattura['numero'];
		
		$data_array = explode("-", $fattura['data']);
		$data = $data_array[2] . "/" . $data_array[1] . "/" . $data_array[0];
		
	}else{
	
		if(isset($_POST['numero'])){
			$numero = $_POST['numero'];
		}else{
			$numero = "n/a";
		}
		
		if(isset($_POST['data'])){
			$data = $_POST['data'];
		}else{
			$data = "n/a";
		}
		
	}
	


	// Controllo se il cliente è stato selezionato e, se si, recupero i dati e li formatto in una lista
	
	//if(isset($id_cliente)){
	if($id_cliente != ""){

		if($cliente['ragione_sociale'] != ""){
			$ragione_sociale = "<li class='evidenza'>".$cliente['ragione_sociale']."</li>";
		}
		
		if($cliente['via'] != "" || $cliente['citta'] != "" ){
			
			$open_li = "<li class='margine-separatore'>";
			$close_li = "</li>";
			
			if($cliente['via'] != ""){
				$via = $cliente['via'];
			}else{
				$via = "";
			}
			
			if($cliente['civico'] != 0){
				$civico = ", ".$cliente['civico'];
			}else{
				$civico = "";
			}
			
			if($cliente['cap'] != 0){
				$cap = $cliente['cap'] . " - ";
			}else{
				$cap = "";
			}
			
			if($cliente['citta'] != ""){
				$citta = $cliente['citta'];
			}else{
				$citta = "";
			}
			
			if($cliente['provincia'] != ""){
				$provincia = "(<span class='maiuscolo'>".$cliente['provincia']."</span>)";
			}else{
				$provincia = "";
			}
			
			$indirizzo_cliente =
				$open_li .
				$via . $civico . " - " .
				$cap . $citta . " " . $provincia .
				$close_li;
		}else{
			$indirizzo_cliente = "";
		}
		
		if($cliente['partita_iva'] != 0){
			$partita_iva = "<li>P.I.: ".$cliente['partita_iva']."</li>";
		}else{
			$partita_iva = "";
		}
		
		if($cliente['codice_fiscale'] != ""){
			$codice_fiscale = "<li>C.F.:".$cliente['codice_fiscale']."</li>";
		}else{
			$codice_fiscale = "";
		}
		
		if($cliente['telefono_fisso'] != ""){
			$telefono_fisso = "<li>Tel: ".$cliente['telefono_fisso']."</li>";
		}else{
			$telefono_fisso = "";
		}
		
		if($cliente['telefono_mobile'] != ""){
			$telefono_mobile = "<li>Cell: ".$cliente['telefono_mobile']."</li>";
		}else{
			$telefono_mobile = "";
		}
		
		if($cliente['fax'] != ""){
			$fax = "<li>FAX: ".$cliente['fax']."</li>";
		}else{
			$fax = "";
		}
		
		if($cliente['email'] != ""){
			$email = "<li>Email: ".$cliente['email']."</li>";
		}else{
			$email = "";
		}
		
		if(strlen($cliente['sito_web']) > 7){
			$sito = "<li>Sito web: ".$cliente['sito_web']."</li>";
		}else{
			$sito = "";
		}
		
		if($cliente['email'] != ""){
			$email = "<li>Email: ".$cliente['email']."</li>";
		}else{
			$email = "";
		}
		
		if($cliente['skype'] != ""){
			$skype = "<li>Skype: ".$cliente['skype']."</li>";
		}else{
			$skype = "";
		}

		$is_client = "<ul>".
			$ragione_sociale .
			$indirizzo_cliente .
			$partita_iva .
			$codice_fiscale .
			$telefono_fisso .
			$telefono_mobile .
			$fax .
			$email .
			$sito .
			$skype .
		"</ul>";

	}else{
		$is_client = "<span class='txt-error'>Non è stato selezionato nessun cliente.</span>";
	}
	
	/****************fine****************/
	/* FORMATTAZIONE ANAGRAFICA CLIENTE */
	/****************fine****************/
	
	
	


	
?>


<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h1>
				<i class="fa fa-file-text"></i>Riepilogo Fattura
			</h1>
		</div>
	</div>
</div>

<div class="container riepilogo-fattura">

	<section>

		<div class="row">
			<div class="col-md-6 col-sm-6">
			
				<div class="logo">
					<img src="uploads/<?php echo $layout['logo']; ?>?v=<?php echo time() ?>" />
				</div>
				
				<div class="anagrafica">
					<?php echo $is_admin; ?>
				</div>
				
			</div>
			<div class="col-md-6 col-sm-6">
				<div class="numero-fattura">
					Fattura numero: <?php echo $numero; ?>
				</div>
				<div class="data-fattura">
					Del: <?php echo $data; ?>
				</div>
				<div class="dati-cliente">
					Spett.<br />
					<?php echo $is_client; ?>
				</div>
			</div>
		</div>

	</section>
	
	<section class="riepilogo-item-row">
	
		<div class="row">
			<div class="col-md-8 col-sm-8">
				SERVIZIO
			</div>
			<div class="col-md-4 col-sm-4">
				PREZZO €
			</div>
		</div>
	
		<?php
			$subtotale = 0;
			$totale = 0;
			$prezzi = array();
			if(isset($elenco_servizi[0])){
			
				foreach($elenco_servizi as $i => $servizio){
					if($servizio != ""){
					
						echo "<div class='row'>";
						echo "<div class='col-md-8 col-sm-8'>";
						echo $servizio;
						echo "</div>";
						echo "<div class='col-md-4 col-sm-4'>";
		
		
						// Controllo che il numero sia stato scritto con la virgola, il punto o niente
						if(strpos($elenco_prezzi[$i], ',') || strpos($elenco_prezzi[$i], '.')){
							
							// Se ha la virgola la sostituisco con il punto, altrimenti non esegue nulla
							$prezzo_num = str_replace(',', '.', $elenco_prezzi[$i]);
							$prezzo_num = round($prezzo_num, 2);
						}else{
							$prezzo_num = $elenco_prezzi[$i];	
						}

						// Controllo che l'inserimento del prezzo comprenda solo numeri, virgole e punti
						if(!is_numeric($prezzo_num)){
							$prezzo_num = 0;
						}
						
		
						$prezzo_str = number2string($prezzo_num);
						/*
						if(preg_match("/[0-9,.]+$/", $prezzo_num)){
							$prezzo_str = number2string($prezzo_num);
						}else{
							$prezzo_str = "0,00";
						}
						*/
						
						echo $prezzo_str ;
						echo "</div>";
						echo "</div>";
						
						$prezzi[] = $prezzo_str;
						$subtotale = $subtotale + $prezzo_num;
	
					}
				}
				
				$totale = $subtotale;
				
				
				// Calcolo l'IVA e l'agiungo al totale.
				if($fisco['iva'] == 1){
					$iva = round($subtotale * $fisco['percent_iva'] / 100, 2);
					$totale = $totale + $iva;
					$iva = number2string($iva);
				}

				
				// Controllo se è previsto il bollo e lo aggiungo al totale.
				if($fisco['bollo'] == 1 && $subtotale >= $fisco['soglia_bollo']){
					$bollo = round($fisco['costo_bollo'] , 2);
					$totale = $totale + $bollo;
					$bollo = number2string($bollo);
				}

				$subtotale_str = number2string($subtotale);
				$totale_num = $totale;
				$totale = number2string($totale);
				
			}
		?>

	</section>
	
	<section class="riepilogo-totali">
	
		<?php
			if($fisco['iva'] == 1 || ($fisco['bollo'] == 1 && $subtotale >= $fisco['soglia_bollo'])){
		?>
			<div class="row subtotale">
				<div class="col-md-8 col-sm-8">
					Subtotale
				</div>
				<div class="col-md-4 col-sm-4">
					<div class="importo-totali">
						<?php echo $subtotale_str ?>
					</div>
				</div>
			</div>
		<?php
			}
		?>
		
		<?php if($fisco['iva'] == 1){ ?>
			<div class="row">
				<div class="col-md-8 col-sm-8">
					IVA (<?php echo $fisco['percent_iva'] ?>%)
				</div>
				<div class="col-md-4 col-sm-4">
					<div class="importo-totali">
						<?php echo $iva ?>
					</div>
				</div>
			</div>
		<?php } ?>
		
		<?php if($fisco['bollo'] == 1 && $subtotale >= $fisco['soglia_bollo']){ ?>
			<div class="row">
				<div class="col-md-8 col-sm-8">
					Bollo
				</div>
				<div class="col-md-4 col-sm-4">
					<div class="importo-totali">
						<?php echo $bollo ?>
					</div>
				</div>
			</div>
		<?php } ?>
		
		<div class="row">
			<div class="col-md-8 col-sm-8">
				TOTALE
			</div>
			<div class="col-md-4 col-sm-4">
				<div class="importo-totali">
					<?php echo $totale ?>
				</div>
			</div>
		</div>
	</section>
	
	



	
	
		<section class="bollo-iban">
		
			<div class="row">
				<div class="col-md-6 col-sm-6 dicitura-bollo">
				<?php
					if(isset($_POST['bollo'])){
						echo $fisco['dicitura_bollo'];
						if(!empty($_POST['idbollo'])){
							echo "<br />ID: " . $_POST['idbollo'];
						}
					}
				?>
				</div>
				<div class="col-md-6 col-sm-6 dicitura-iban">
					<?php
						if($fisco['iban'] != "" && $fisco['intestazione_cc'] != ""){
							echo "
								Pagamento a mezzo bonifico bancario<br />
								Intestato a: " . $fisco['intestazione_cc'] . "<br />
								IBAN: " . $fisco['iban']  . "
							";
						}
					?>
				</div>
			</div>
	
		</section>
	
	
	
	
	
	
	<?php if($layout['note'] != ""){ ?>
	
		<section class="note-in-fattura">
		
			<div class="row">
				<div class="col-md-12">
					<?php echo $layout['note']; ?>
				</div>
			</div>
	
		</section>
	
	<?php } ?>

</div>

<div class="container">
	<div class="row">
		<div class="col-md-12">
		
		
			<?php if($modmode != 1){ ?>
			
				<form action="class/class-fattura-pdf.php" method="post" target="_blank">
					<input type="hidden" name="numero" value="<?php echo $numero; ?>" />
					<input type="hidden" name="data" value="<?php echo $data; ?>" />
					<input type="hidden" name="cliente" value="<?php echo $id_cliente; ?>" />
					
					<?php foreach($elenco_servizi as $i => $servizio){ ?>
						<input type="hidden" name="servizi[]" value="<?php echo $servizio; ?>" />
						<input type="hidden" name="prezzi[]" value="<?php echo $prezzi[$i]; ?>" />
					<?php }	?>
					
					<?php if(isset($subtotale)){ ?>
						<input type="hidden" name="subtotale" value="<?php echo $subtotale_str ?>" />
					<?php } ?>
						
					<?php if(isset($iva)){ ?>
						<input type="hidden" name="iva" value="<?php echo $iva ?>" />
					<?php } ?>
					
					<?php if(isset($_POST['bollo'])){ ?>
						<input type="hidden" name="bollo" value="<?php echo $bollo ?>" />
						<input type="hidden" name="idbollo" value="<?php echo $id_bollo; ?>" />
					<?php } ?>
					
					<input type="hidden" name="totale" value="<?php echo $totale ?>" />
					<input type="hidden" name="totale_num" value="<?php echo $totale_num ?>" />
	
	
					<button type="submit" class="btn-lg btn-success" id="submitCreaFattura">
						CREA FATTURA
					</button>
				</form>
				oppure<br />
				
			<?php } ?>
				
				
			<form method="post" action="index.php?page=nuova-fattura">
				<input type="hidden" name="mod_fattura" value="1" />
				
				<input type="hidden" name="numero" value="<?php echo $numero; ?>" />
				<input type="hidden" name="data" value="<?php echo $data; ?>" />
				<input type="hidden" name="cliente" value="<?php echo $id_cliente; ?>" />
				
				<?php foreach($elenco_servizi as $i => $servizio){ ?>
					<input type="hidden" name="servizi[]" value="<?php echo $servizio; ?>" />
					<input type="hidden" name="prezzi[]" value="<?php echo $prezzi[$i]; ?>" />
				<?php }	?>
				
				<?php if(isset($bollo_check)){ ?>
					<input type="hidden" name="bollo" value="<?php echo $bollo_check ?>" />
					<input type="hidden" name="idbollo" value="<?php echo $id_bollo; ?>" />
				<?php } ?>

				<button type="submit" class="<?php if($modmode == 1){echo "btn-lg btn-success";}else{echo "button-link";} ?>">
					modifica fattura
				</button>
			</form>
		</div>
	</div>
</div>
	