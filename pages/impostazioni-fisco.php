<?php
	$query = "SELECT * FROM fisco WHERE role='admin'";
	$result = $conn->query($query);
	$row = $result->fetch_array();
	
	if(isset($row['iva'])){
		if($row['iva'] == 1){
			$sel_iva_si = "checked";
			$sel_iva_no = "";
		}else if($row['iva'] == 0){
			$sel_iva_si = "";
			$sel_iva_no = "checked";
		}else{
			$sel_iva_si = "";
			$sel_iva_no = "";
		}
	}
	
	if(isset($row['ritenuta'])){
		if($row['ritenuta'] == 1){
			$sel_ritenuta_si = "checked";
			$sel_ritenuta_no = "";
		}else if($row['ritenuta'] == 0){
			$sel_ritenuta_si = "";
			$sel_ritenuta_no = "checked";
		}else{
			$sel_ritenuta_si = "";
			$sel_ritenuta_no = "";
		}
	}
	
	if(isset($row['bollo'])){
		if($row['bollo'] == 1){
			$sel_bollo_si = "checked";
			$sel_bollo_no = "";
		}else if($row['bollo'] == 0){
			$sel_bollo_si = "";
			$sel_bollo_no = "checked";
		}else{
			$sel_bollo_si = "";
			$sel_bollo_no = "";
		}
	}
	
	if($row['percent_iva'] > 0){
		$percent_iva = $row['percent_iva'];
	}else{
		$percent_iva = "";
	}
	
	if($row['percent_ritenuta'] > 0){
		$percent_ritenuta = $row['percent_ritenuta'];
	}else{
		$percent_ritenuta = "";
	}
	
	if($row['soglia_bollo'] > 0){
		$soglia_bollo = $row['soglia_bollo'];
	}else{
		$soglia_bollo = "";
	}
	
	if($row['costo_bollo'] > 0){
		$costo_bollo = $row['costo_bollo'];
	}else{
		$costo_bollo = "";
	}
	
	// echo "<pre>" . print_r($row,1) . "</pre>";

?>
<div class="impostazioni">

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>
					<i class="fa fa-cog"></i>Fisco
				</h1>
			</div>
		</div>
	</div>
	
		
	<form method="post" action="class/class-impostazioni-fisco.php">

		<section>

			<div class="container">
				<div class="row">
				
					<div class="col-md-2 col-sm-3 col-xs-6">
						<div class="form-group">
							<label>IVA</label>
							<div class="radio">
								<label>
									<input type="radio" class="ck_iva" name="iva" id="ivasi" value="si" <?php echo $sel_iva_si ?>>
									Si
								</label>
							</div>
							<div class="radio">
								<label>
									<input type="radio" class="ck_iva" name="iva" id="ivano" value="no" <?php echo $sel_iva_no ?>>
									No
								</label>
							</div>
						</div>
					</div>
					
					<div class="col-md-2 col-sm-3 col-xs-6">
						<div class="form-group">
							<label for="admin-percent-iva">% IVA</label>
							<input type="text" class="form-control" id="admin-percent-iva" name="percent-iva" value="<?php echo $percent_iva ?>">
						</div>
					</div>
					
					<div class="col-md-2 col-md-offset-2 col-sm-3 col-xs-6">
						<div class="form-group">
							<label>Ritenuta d'acconto</label>
							<div class="radio">
								<label>
									<input type="radio" class="ck_ritenuta" name="ritenuta" id="ritenutasi" value="si" <?php echo $sel_ritenuta_si ?>>
									Si
								</label>
							</div>
							<div class="radio">
								<label>
									<input type="radio" class="ck_ritenuta" name="ritenuta" id="ritenutano" value="no" <?php echo $sel_ritenuta_no ?>>
									No
								</label>
							</div>
						</div>
					</div>
					
					<div class="col-md-2 col-sm-3 col-xs-6">
						<div class="form-group">
							<label for="admin-percent-ritenuta">% Ritenuta</label>
							<input type="text" class="form-control" id="admin-percent-ritenuta" name="percent-ritenuta" value="<?php echo $percent_ritenuta ?>">
						</div>
					</div>
					
				</div>
			</div>

		</section>
		
		
		
		<section>
	
			<div class="container">
			
				<div class="row">
				
					<div class="col-md-12">
						<h3 class="title-section">
							Bollo
						</h3>
					</div>
				
					<div class="col-md-2 col-sm-2 col-xs-5">
						<div class="form-group">
							<label>Bollo</label>
							<div class="radio">
								<label>
									<input type="radio" class="ck_bollo" name="bollo" id="bollosi" value="si" <?php echo $sel_bollo_si ?>>
									Si
								</label>
							</div>
							<div class="radio">
								<label>
									<input type="radio" class="ck_bollo" name="bollo" id="bollono" value="no" <?php echo $sel_bollo_no ?>>
									No
								</label>
							</div>
						</div>
					</div>
					
					<div class="col-md-2 col-sm-2 col-xs-4">
						<div class="form-group">
							<label for="admin-soglia-bollo">Soglia</label>
							<input type="text" class="form-control" id="admin-soglia-bollo" name="soglia-bollo" value="<?php echo $soglia_bollo ?>">
						</div>
					</div>
					
					<div class="col-md-1 col-sm-2 col-xs-3">
						<div class="form-group">
							<label for="admin-costo-bollo">Costo</label>
							<input type="text" class="form-control" id="admin-costo-bollo" name="costo-bollo" value="<?php echo $costo_bollo ?>">
						</div>
					</div>
					
					<div class="col-md-7 col-sm-6 col-xs-12">
						<div class="form-group">
							<label for="admin-dcitura-bollo">Dicitura se assolto all'origine</label>
							<input type="text" class="form-control" id="admin-dicitura-bollo" name="dicitura-bollo" value="<?php echo $row['dicitura_bollo'] ?>">
						</div>
					</div>

				
				</div>
			
			</div>

		</section>
		
		
		
		<section>

			<div class="container">
				<div class="row">
				
					<div class="col-md-12">
						<h3 class="title-section">
							Bonifico
						</h3>
					</div>
					
					<div class="col-md-6">
						<div class="form-group">
							<label for="admin-intestazione-cc">Intestato a</label>
							<input type="text" class="form-control" id="admin-intestazione-cc" name="intestazione-cc" value="<?php echo $row['intestazione_cc'] ?>">
						</div>
					</div>
				
					<div class="col-md-6">
						<div class="form-group">
							<label for="admin-iban">IBAN</label>
							<input type="text" class="form-control" id="admin-iban" name="iban" value="<?php echo $row['iban'] ?>">
						</div>
					</div>
				</div>
			</div>

		</section>
		
		

		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<button type="submit" class="btn-lg btn-success">
						SALVA DATI FISCALI
					</button>
				</div>
			</div>
		
		</div>

	</form>
	
</div>