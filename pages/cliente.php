<?php
	$query = "SELECT * FROM clienti WHERE id=" . $_GET['id'];
	$result = $conn->query($query);
	$row = $result->fetch_array();
?>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h1>
				<i class="fa fa-user"></i><?php
					if($row['ragione_sociale'] != ""){
							echo $row['ragione_sociale'];
						}
				?>
			</h1>
		</div>
	</div>
</div>

<section class="cliente">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12 cliente-block">
				<ul class="resetlist">
					<li>
						<h3 class="title-section">
							Anagrafica
						</h3>
					</li>
					<?php
						if($row['partita_iva'] != ""){
							echo "<li><span class='etichetta-cliente'>P.I.</span>".$row['partita_iva']."</li>";
						}
						if($row['codice_fiscale'] != ""){
							echo "<li><span class='etichetta-cliente'>C.F.</span>".$row['codice_fiscale']."</li>";
						}
					?>
					<li class="spacer"></li>
					<?php
					
						if($row['via'] != ""){
							echo "<li><span class='etichetta-ico'><i class='fa fa-map-marker'></i></span>".$row['via']." ".$row['civico']." - ".$row['cap']." - ".$row['citta']." (<span class='maiuscolo'>".$row['provincia']."</span>)</li>";
						}
						if(strlen($row['sito_web']) > 7){
							echo "<li><span class='etichetta-ico'><i class='fa fa-globe'></i></span><a href='".addhttp($row['sito_web'])."' target='_blank'>".$row['sito_web']."</a></li>";
						}
						if($row['email'] != ""){
							echo "<li><span class='etichetta-ico'><i class='fa fa-envelope-o'></i></span><a href='mailto:".$row['email']."'>".$row['email']."</a></li>";
						}
						if($row['telefono_fisso'] != ""){
							echo "<li><span class='etichetta-ico'><i class='fa fa-phone'></i></span><a href='tel:".$row['telefono_fisso']."'>".$row['telefono_fisso']."</a></li>";
						}
						if($row['telefono_mobile'] != ""){
							echo "<li><span class='etichetta-ico'><i class='fa fa-mobile'></i></span><a href='tel:".$row['telefono_mobile']."'>".$row['telefono_mobile']."</a></li>";
						}
						if($row['fax'] != ""){
							echo "<li><span class='etichetta-ico'><i class='fa fa-fax'></i></span><a href='tel:".$row['fax']."'>".$row['fax']."</a></li>";
						}
						if($row['skype'] != ""){
							echo "<li><span class='etichetta-ico'><i class='fa fa-skype'></i></span><a href='skype:".$row['skype']."'>".$row['skype']."</a></li>";
						}
					
					?>
				</ul>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12 cliente-block">
				<ul class="resetlist">
					<li>
						<h3 class="title-section">
							Referente
						</h3>
					</li>
					<?php
					
						if($row['nome_referente'] != ""){
							echo "<li><span class='etichetta-ico'><i class='fa fa-user'></i></span>".$row['nome_referente']."</li>";
						}
						if($row['ruolo_referente'] != ""){
							echo "<li><span class='etichetta-ico'><i class='fa fa-suitcase'></i></span>".$row['ruolo_referente']."</li>";
						}
					?>
					<li class="spacer"></li>
					
						<?php
						if($row['email_referente'] != ""){
							echo "<li><span class='etichetta-ico'><i class='fa fa-envelope-o'></i></span><a href='mailto:".$row['email_referente']."'>".$row['email_referente']."</a></li>";
						}
						if($row['telefono_referente'] != ""){
							echo "<li><span class='etichetta-ico'><i class='fa fa-phone'></i></span><a href='tel:".$row['telefono_referente']."'>".$row['telefono_referente']."</a></li>";
						}
						if($row['skype_referente'] != ""){
							echo "<li><span class='etichetta-ico'><i class='fa fa-skype'></i></span><a href='skype:".$row['skype_referente']."'>".$row['skype_referente']."</a></li>";
						}
					?>
				</ul>
			</div>
		</div>
	</div>
</section>

<div class="container">

	<div class="row">
		<div class="col-md-12">
			<a href="?page=modifica-cliente&id=<?php echo $_GET['id'] ?>" class="btn-lg btn-success">
				MODIFICA CLIENTE
			</a>
		</div>
	</div>

</div>

