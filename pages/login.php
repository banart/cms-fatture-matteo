<?php
	if(isset($_SESSION['error_login_email']) && $_SESSION['error_login_email'] !== 0){
	
		$err_email = "field-error";
		
		if($_SESSION['error_login_email'] == 1){
			$err_email_mess = '<div class="mess-error">L\'indirizzo email è errato o non esistente!</div>';
		}elseif($_SESSION['error_login_email'] == "null"){
			$err_email_mess = '<div class="mess-error">Non hai inserito l\'indirizzo email!</div>';
		}else{
			$err_email_mess = "";
		}
		
	}else{
		$err_email = "";
	}
	
	if(isset($_SESSION['error_login_password']) && $_SESSION['error_login_password'] !== 0){
	
		$err_pass ="field-error";
		
		if($_SESSION['error_login_password'] == 1){
			$err_pass_mess = '<div class="mess-error">La password è errata!</div>';
		}elseif($_SESSION['error_login_password'] == "null"){
			$err_pass_mess = '<div class="mess-error">Non hai inserito la password!</div>';
		}else{
			$err_pass_mess = "";
		}
		
	}else{
		$err_pass = "";
	}
	
	if(isset($_SESSION['login_email'])){
		$value_email = "value=\"" . $_SESSION['login_email'] . "\"";
	}else{
		$value_email = "";
	}
?>

<div class="container login-form">
	<div class="row">
		<div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
			<h2>
				<i class="fa fa-lock"></i> Login
			</h2>
			

			<form class="form-horizontal" action="class/class-login.php" method="post">
			
				<div class="form-group">
					<div class="col-md-12">
						<input type="email" name="email" class="form-control <?php echo $err_email ?>" placeholder="Email" <?php echo $value_email ?>>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-md-12">
						<input type="password" name="password" class="form-control <?php echo $err_pass ?>" placeholder="Password">
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-md-12">
						<button type="submit" class="btn btn-success">Entra</button>
					</div>
				</div>
			
			</form>
			
			
			
			<?php
				if(isset($err_email_mess)){
					echo $err_email_mess;
				}
				
				if(isset($err_pass_mess)){
					echo $err_pass_mess;
				}
			?>
			
			
			<form method="post" action="index.php">
				<input type="hidden" name="send_reset_password" value="1" />
				<button type="submit" class="button-link">
					Hai dimenticato la password?
				</button>
			</form>

		</div>
	</div>
</div>

<?php
	$_SESSION['error_login_email'] = 0;
	$_SESSION['error_login_password'] = 0;
?>