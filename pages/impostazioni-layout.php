<?php
	$query = "SELECT * FROM layout WHERE role = 'admin'";
	$result = $conn->query($query);
	$layout = $result->fetch_array();
?>

<div class="impostazioni">

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>
					<i class="fa fa-cog"></i>Layout fattura
				</h1>
			</div>
		</div>
	</div>


	<section>

		<div class="container">
			<div class="row">
			
				<div class="col-md-6 col-sm-6 col-xs-12">
				
					<form enctype="multipart/form-data" method="POST" action="class/class-impostazioni-logo.php">
						<div class="form-group">
							<label for="logofattura">Logo</label>
							<img class="prev-logo" src="uploads/logo.png?v=<?php echo time() ?>" />
							<br /><br />
							<?php
								if(isset($_SESSION['logo_error_message']) && $_SESSION['logo_error_message'] != ""){
									echo '<div class="logo-error-message">';
									echo $_SESSION['logo_error_message'];
									echo '</div>';
									unset($_SESSION['logo_error_message']);
								}
							?>
							<input type="file" name="logofattura" id="logofattura">
							<div class="indicazioni-logo">
								Puoi caricare file JPG, GIF e PNG.<br />
								L'immagine dovrà essere al massimo di 1 MB.<br />
								Le dimensioni consigliate sono 350px x 100px.
							</div>
							<button type="submit" class="btn-lg btn-success">
								CARICA IMMAGINE
							</button>
						</div>
					</form>
					
				</div>
				
				<div class="col-md-6 col-sm-6 col-xs-12">
					<label for="logofattura">Colore</label>
					
					<div class="colors-block">
						<form method="post" action="class/class-impostazioni-layout.php">
							<div class="color-block__list">
								<div class="colors-single">
									<input type="radio" name="color" id="337ab7" value="337ab7" <?php if($layout['colore'] == "337ab7"){echo "checked";} ?> />
									<label for="337ab7" style="background-color: #337ab7;"></label>
								</div>
								<div class="colors-single">
									<input type="radio" name="color" id="ff7200" value="ff7200" <?php if($layout['colore'] == "ff7200"){echo "checked";} ?> />
									<label for="ff7200" style="background-color: #ff7200;"></label>
								</div>
								<div class="colors-single">
									<input type="radio" name="color" id="638254" value="638254" <?php if($layout['colore'] == "638254"){echo "checked";} ?> />
									<label for="638254" style="background-color: #638254;"></label>
								</div>
								<div class="colors-single">
									<input type="radio" name="color" id="993399" value="993399" <?php if($layout['colore'] == "993399"){echo "checked";} ?> />
									<label for="993399" style="background-color: #993399;"></label>
								</div>
								<div class="colors-single">
									<input type="radio" name="color" id="cc3333" value="cc3333" <?php if($layout['colore'] == "cc3333"){echo "checked";} ?> />
									<label for="cc3333" style="background-color: #cc3333;"></label>
								</div>
								<div class="colors-single">
									<input type="radio" name="color" id="336666" value="336666" <?php if($layout['colore'] == "336666"){echo "checked";} ?> />
									<label for="336666" style="background-color: #336666;"></label>
								</div>
								<div class="colors-single">
									<input type="radio" name="color" id="000000" value="000000" <?php if($layout['colore'] == "000000"){echo "checked";} ?> />
									<label for="000000" style="background-color: #000000;"></label>
								</div>
								<div class="colors-single">
									<input type="radio" name="color" id="666666" value="666666" <?php if($layout['colore'] == "666666"){echo "checked";} ?> />
									<label for="666666" style="background-color: #666666;"></label>
								</div>
							</div>
							<div class="clearfix"></div>
							<button type="submit" class="btn-lg btn-success">
								SALVA COLORE
							</button>
						</form>
					</div>
				</div>

			</div>
		</div>

	</section>
	
	
	<form method="post" action="class/class-impostazioni-layout.php">
	
		<section>
	
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="admin-ragione-sociale">Note in calce</label>
							<textarea type="text" class="form-control note" id="note-fatura" name="note-fatura"><?php echo $layout['note'] ?></textarea>
						</div>
					</div>
				</div>
			</div>
	
		</section>
		
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<button type="submit" class="btn-lg btn-success">
						SALVA
					</button>
				</div>
			</div>
		</div>

	</form>

	
</div>