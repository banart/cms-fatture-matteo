<div class="impostazioni">

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>
					<i class="fa fa-unlock-alt"></i>Cambio password
				</h1>
			</div>
		</div>
	</div>
	
		
	<form method="post" action="class/class-modifica-password.php">

		<section>

			<div class="container">
				<div class="row">
				
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<label>Vecchia password</label>
							<input type="password" class="form-control" name="password-old" placeholder="">
						</div>
					</div>

				</div>
			</div>

		</section>
		
		
		
		<section>
	
			<div class="container">
			
				<div class="row">
				
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<label>Nuova password</label>
							<input type="password" class="form-control" name="password-new1" placeholder="">
						</div>
					</div>
					
					
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<label>Ripeti password</label>
							<input type="password" class="form-control" name="password-new2" placeholder="">
						</div>
					</div>

				
				</div>
			
			</div>

		</section>		
		

		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<button type="submit" class="btn-lg btn-success">
						SALVA PASSWORD
					</button>
				</div>
			</div>
		
		</div>

	</form>
	
</div>