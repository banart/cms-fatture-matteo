<?php
	$query = "SELECT * FROM anagrafica WHERE role='admin'";
	$result = $conn->query($query);
	$row = $result->fetch_array();
	
	$query = "SELECT * FROM province ORDER BY nome ASC";
	$result = $conn->query($query);
	$prv = $result->fetch_array();
?>

<div class="impostazioni">

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>
					<i class="fa fa-cog"></i>Anagrafica
				</h1>
			</div>
		</div>
	</div>
	
		
	<form method="post" action="class/class-impostazioni-anagrafica.php">

		<section>

			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label for="admin-ragione-sociale">Ragione Sociale</label>
							<input type="text" class="form-control" id="admin-ragione-sociale" name="admin-ragione-sociale" value="<?php echo $row['ragione_sociale'] ?>">
						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="form-group">
							<label for="admin-partita-iva">Partita IVA</label>
							<input type="text" class="form-control" id="admin-partita-iva" name="admin-partita-iva" value="<?php echo $row['partita_iva'] ?>">
						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="form-group">
							<label for="admin-codice-fiscale">Codice Fiscale</label>
							<input type="text" class="form-control" id="admin-codice-fiscale" name="admin-codice-fiscale" value="<?php echo $row['codice_fiscale'] ?>">
						</div>
					</div>
				</div>
			</div>

		</section>
		
		
		
		<section>
	
			<div class="container">
			
				<div class="row">
				
					<div class="col-md-12">
						<h3 class="title-section">
							Indirizzo
						</h3>
					</div>
				
					<div class="col-md-4 col-sm-10 col-xs-9">
						<div class="form-group">
							<label for="admin-via">Via</label>
							<input type="text" class="form-control" id="admin-via" name="admin-via" value="<?php echo $row['via'] ?>">
						</div>
					</div>
					
					<div class="col-md-1 col-sm-2 col-xs-3">
						<div class="form-group">
							<label for="admin-numero-civico">Civico</label>
							<input type="text" class="form-control" id="admin-civico" name="admin-civico" value="<?php echo $row['civico'] ?>">
						</div>
					</div>
					
					<div class="col-md-2 col-sm-3 col-xs-4">
						<div class="form-group">
							<label for="admin-cap">CAP</label>
							<input type="text" class="form-control" id="admin-cap" name="admin-cap" value="<?php echo $row['cap'] ?>">
						</div>
					</div>
					
					<div class="col-md-2 col-sm-4 col-xs-8">
						<div class="form-group">
							<label for="admin-citta">Città</label>
							<input type="text" class="form-control" id="admin-citta" name="admin-citta" value="<?php echo $row['citta'] ?>">
						</div>
					</div>
					
					<div class="col-md-3 col-sm-5 col-xs-12">
						<div class="form-group add-client">
							<label for="admin-provincia">
								Provincia
							</label>
							<div class="styled-select">
								<select class="form-control" name="admin-provincia" id="admin-provincia">
									<option></option>
									<?php
										while($prv = $result->fetch_array()){
											if ($prv['sigla'] == $row['provincia']){
												$sel = ' selected="selected"';
											}else{
												$sel = '';
											}
											echo '<option value="'.$prv['sigla'].'"'.$sel.'>'.$prv['nome'].'</option>';
										}
									?>
								</select>
							</div>
							<!-- <a href="#"><i class="fa fa-plus"></i></a> -->
						</div>
					</div>
				
				</div>
			
			</div>

		</section>
		
		
		<section>
			<div class="container">
				<div class="row">
				
					<div class="col-md-12">
						<h3 class="title-section">
							Contatti
						</h3>
					</div>
				
					<div class="col-md-4 col-sm-4 col-xs-6">
						<div class="form-group">
							<label for="admin-sito">Sito web</label>
							<input type="text" class="form-control" id="admin-sito-web" name="admin-sito-web" value="<?php echo $row['sito_web'] ?>">
						</div>
					</div>
					
					<div class="col-md-4 col-sm-4 col-xs-6">
						<div class="form-group">
							<label for="admin-email">E-mail</label>
							<input type="text" class="form-control" id="admin-email" name="admin-email" value="<?php echo $row['email'] ?>">
						</div>
					</div>
					
					<div class="col-md-4 col-sm-4 col-xs-6">
						<div class="form-group">
							<label for="admin-telefono">Telefono fisso</label>
							<input type="text" class="form-control" id="admin-telefono-fisso" name="admin-telefono-fisso" value="<?php echo $row['telefono_fisso'] ?>">
						</div>
					</div>
					
					<div class="col-md-4 col-sm-4 col-xs-6">
						<div class="form-group">
							<label for="admin-cellulare">Telefono mobile</label>
							<input type="text" class="form-control" id="admin-telefono-fisso" name="admin-telefono-mobile" value="<?php echo $row['telefono_mobile'] ?>">
						</div>
					</div>
					
					<div class="col-md-4 col-sm-4 col-xs-6">
						<div class="form-group">
							<label for="admin-fax">FAX</label>
							<input type="text" class="form-control" id="admin-fax" name="admin-fax" value="<?php echo $row['fax'] ?>">
						</div>
					</div>
					
					<div class="col-md-4 col-sm-4 col-xs-6">
						<div class="form-group">
							<label for="admin-skype">Skype</label>
							<input type="text" class="form-control" id="admin-skype" name="admin-skype" value="<?php echo $row['skype'] ?>">
						</div>
					</div>
					
				</div>
			</div>
		</section>
		
		

		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<button type="submit" class="btn-lg btn-success">
						SALVA ANAGRAFICA
					</button>
				</div>
			</div>
		
		</div>

	</form>
	
</div>