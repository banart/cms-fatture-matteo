<?php
	
	// echo "<pre>" . print_r($_POST,1) . "</pre>";
	
	$path = "../pdf/";
	$destination = "../zip/";
	$zipname = date("Ymd_His").".zip";
	
	$zip = new ZipArchive;
	$zip->open($destination.$zipname, ZipArchive::CREATE);
	
	foreach($_POST['zipfatture'] as $a => $fattura) {
		$file = $path.$fattura.".pdf";
		$zip->addFile($file);
	}
	$zip->close();
	
	header('Content-Type: application/zip');
	header('Content-disposition: attachment; filename='.$zipname);
	header('Content-Length: ' . filesize($destination.$zipname));
	readfile($destination.$zipname);
	
?>