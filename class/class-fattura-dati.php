<?php
	include('connect.php');
	include('../includes/functions.php');
	$query = "SELECT * FROM anagrafica WHERE role = 'admin'";
	$result = $conn->query($query);
	$anagrafica = $result->fetch_array();
	
	$query = "SELECT * FROM layout WHERE role = 'admin'";
	$result = $conn->query($query);
	$layout = $result->fetch_array();
	
	$query = "SELECT * FROM clienti WHERE id=" . $_POST['cliente'];
	$result = $conn->query($query);
	$cliente = $result->fetch_array();
	
	$query = "SELECT * FROM fisco WHERE role = 'admin'";
	$result = $conn->query($query);
	$fisco = $result->fetch_array();
	
	$base_color = "#".$layout['colore'];
	$rgb_color = hex2rgb($base_color);
	$light_color = rgba2hex("rgba(" . $rgb_color[0] . "," . $rgb_color[1] . "," . $rgb_color[2] . ",0.08)");
	
?>


<style type="text/css">
<!--
table
{
    width:  100%;
    /*border: solid 1px #5544DD;*/
    color: #333333;
}

th
{
    /*text-align: center;
    border: solid 1px #113300;*/
    /*background: #EEFFEE;*/
    font-weight: normal;
}

td
{
    /*text-align: left;
    border: solid 1px #55DD44;*/
}

td.col1
{
   /* border: solid 1px red;
    text-align: right;*/
}

.logo{

}

.right-head{
	border-left: 2px solid <?php echo $base_color ?>;
}

.data-numero{
	padding: 20px;
	font-size: 18px;
}

.anagrafica-cliente{
	background: <?php echo $light_color ?>;
	padding: 20px;
}


-->
</style>
<table>

    <thead>
        <tr>
            <th>
	            <table>
	            	<tr valign="top">
	            		<td style="width:50%">

	            			<table>
	            				<?php if(file_exists('../uploads/'.$layout['logo'])){ ?>
	            				<tr>
	            					<td style="padding-bottom: 10px; width:100%;">
			            				<img class="logo" src="../uploads/<?php echo $layout['logo'] ?>" />
	            					</td>
	            				</tr>
		            			<?php } ?>
	            			
	            				<?php if($anagrafica['ragione_sociale'] != ""){ ?>
			            			<tr>
		            					<td style="width:100%;">
		            						<span style="font-size: 22px; font-width: bold;"><?php echo $anagrafica['ragione_sociale']; ?></span>
		            					</td>
		            				</tr>
		            			<?php } ?>
		            			
		            			<?php if($anagrafica['via'] != "" || $anagrafica['via'] != ""){ ?>
			            			<tr>
		            					<td style="width:100%;">
		            						<?php echo $anagrafica['via']; ?>, <?php echo $anagrafica['civico']; ?> - <?php echo $anagrafica['cap']; ?> - <?php echo $anagrafica['citta']; ?>
		            					</td>
		            				</tr>
		            			<?php } ?>
		            			
		            			<?php if($anagrafica['partita_iva'] != "" && $anagrafica['partita_iva'] != "0"){ ?>
			            			<tr>
		            					<td style="width:100%;">
		            						P.I.: <?php echo $anagrafica['partita_iva']; ?>
		            					</td>
		            				</tr>
		            			<?php } ?>
		            			
		            			<?php if($anagrafica['codice_fiscale'] != ""){ ?>
			            			<tr>
		            					<td style="width:100%;">
		            						C.F.: <?php echo $anagrafica['codice_fiscale']; ?>
		            					</td>
		            				</tr>
		            			<?php } ?>
		            			
		            			<?php if($anagrafica['email'] != ""){ ?>
			            			<tr>
		            					<td style="width:100%;">
		            						<?php echo $anagrafica['email']; ?>
		            					</td>
		            				</tr>
		            			<?php } ?>

		            			<?php if(strlen($anagrafica['sito_web']) > 7){ ?>
			            			<tr>
		            					<td style="width:100%;">
		            						<?php echo $anagrafica['sito_web']; ?>
		            					</td>
		            				</tr>
		            			<?php } ?>
		            			
		            			<?php if($anagrafica['telefono_fisso'] != ""){ ?>
			            			<tr>
		            					<td style="width:100%;">
		            						<?php echo $anagrafica['telefono_fisso']; ?>
		            					</td>
		            				</tr>
		            			<?php } ?>
		            			
		            			<?php if($anagrafica['telefono_mobile'] != ""){ ?>
			            			<tr>
		            					<td style="width:100%;">
		            						<?php echo $anagrafica['telefono_mobile']; ?>
		            					</td>
		            				</tr>
		            			<?php } ?>
	            				
	            			</table>
	            			
	            		</td>
	            		<td style="width:50%">
	            			<table cellpadding="0" cellspacing="0" class="right-head">
	            				<tr>
	            					<td class="data-numero" style="width:100%">
	            						Fattura n° <?php echo $_POST['numero'] ?><br />
	            						del <?php echo $_POST['data'] ?>
	            					</td>
	            				</tr>
	            				<tr>
	            					<td class="anagrafica-cliente" style="width:100%">
	            						<table>
	            						
			            					<?php if($cliente['ragione_sociale'] != ""){ ?>
						            			<tr>
					            					<td style="width:100%;">
					            						Spett.<br />
					            						<span style="font-size: 22px; font-width: bold;"><?php echo $cliente['ragione_sociale']; ?></span>
					            					</td>
					            				</tr>
					            			<?php } ?>
					            			
					            			<?php if($cliente['via'] != "" || $cliente['citta'] != ""){ ?>
						            			<tr>
					            					<td style="width:100%;">
					            						<?php echo $cliente['via'] . ", " .$cliente['civico'] . " - " . $cliente['cap'] . " " . $cliente['citta'] . " (" . $cliente['provincia'] . ")"; ?>
					            					</td>
					            				</tr>
					            			<?php } ?>
					            			
					            			<?php if($cliente['partita_iva'] != ""){ ?>
						            			<tr>
					            					<td style="width:100%;">
					            						P.I.: <?php echo $cliente['partita_iva']; ?>
					            					</td>
					            				</tr>
					            			<?php } ?>
					            			
					            			<?php if($cliente['codice_fiscale'] != ""){ ?>
						            			<tr>
					            					<td style="width:100%;">
					            						C.F.: <?php echo $cliente['codice_fiscale']; ?>
					            					</td>
					            				</tr>
					            			<?php } ?>
					            			
					            			<?php if($cliente['telefono_fisso'] != ""){ ?>
						            			<tr>
					            					<td style="width:100%;">
					            						Tel: <?php echo $cliente['telefono_fisso']; ?>
					            					</td>
					            				</tr>
					            			<?php } ?>
					            			
					            			<?php if($cliente['telefono_mobile'] != ""){ ?>
						            			<tr>
					            					<td style="width:100%;">
					            						Cell: <?php echo $cliente['telefono_mobile']; ?>
					            					</td>
					            				</tr>
					            			<?php } ?>
					            			
					            			<?php if($cliente['fax'] != ""){ ?>
						            			<tr>
					            					<td style="width:100%;">
					            						FAX: <?php echo $cliente['fax']; ?>
					            					</td>
					            				</tr>
					            			<?php } ?>
					            			
					            			<?php if($cliente['email'] != ""){ ?>
						            			<tr>
					            					<td style="width:100%;">
					            						<?php echo $cliente['email']; ?>
					            					</td>
					            				</tr>
					            			<?php } ?>
					            			
					            			<?php if(strlen($cliente['sito_web']) > 7){ ?>
						            			<tr>
					            					<td style="width:100%;">
					            						<?php echo $cliente['sito_web']; ?>
					            					</td>
					            				</tr>
					            			<?php } ?>
					            			
					            			<?php if($cliente['skype'] != ""){ ?>
						            			<tr>
					            					<td style="width:100%;">
					            						Skype: <?php echo $cliente['skype']; ?>
					            					</td>
					            				</tr>
					            			<?php } ?>
					            			
	            						</table>
		          					</td>
	            				</tr>
	            			</table>
	            		</td>
	            	</tr>
	            </table>
            </th>
        </tr>
    </thead>


    <tr>
        <td style="padding: 30px 0 7px;">
	        <table>
	        	<tr>
	        		<td style="width:70%; padding-left: 10px; font-weight: bold;">
		        		SERVIZIO
	        		</td>
	        		<td style="width:30%; text-align:right; padding-right: 10px; font-weight: bold;">
	        			PREZZO €
	        		</td>
	        	</tr>
	        </table>
        </td>
    </tr>

<?php
	foreach($_POST['servizi'] as $i => $servizio){
	if($i & 1){
		$clr = "#ffffff";
	}else{
		$clr = $light_color;
	}
?>
	
    <tr style="background: <?php echo $clr ?>;">
        <td>
	        <table>
	        	<tr>
	        		<td style="width:70%; padding: 5px 0 5px 10px;">
		        		<?php echo $servizio ?>
	        		</td>
	        		<td style="width:30%; text-align:right; padding: 5px 10px 5px 0;">
	        			<?php echo $_POST['prezzi'][$i] ?>
	        		</td>
	        	</tr>
	        </table>
        </td>
    </tr>

<?php } ?>

	
	
<?php if(isset($_POST['iva']) || isset($_POST['bollo'])){ ?>
	<tr><td style="padding: 2px 0; border-bottom: 1px solid #999999;"></td></tr>
	<tr><td style="padding: 2px 0 0 0;"></td></tr>
	<tr>
        <td style="width:100%;">
	        <table>
	        	<tr>
	        		<td style="width:70%; padding-left: 10px; font-weight: bold; font-size: 12px;">
		        		Subtotale
	        		</td>
	        		<td style="width:30%; text-align:right; padding-right: 10px; font-weight: bold;">
	        			<?php echo $_POST['subtotale'] ?>
	        		</td>
	        	</tr>
	        	
	        <?php if(isset($_POST['iva'])){ ?>
	        	<tr>
	        		<td style="width:70%; padding-left: 10px; font-weight: bold; font-size: 12px;">
		        		Iva
	        		</td>
	        		<td style="width:30%; text-align:right; padding-right: 10px; font-weight: bold;">
	        			<?php echo $_POST['iva'] ?>
	        		</td>
	        	</tr>
			<?php } ?>
			
			<?php if(isset($_POST['bollo'])){ ?>
	        	<tr>
	        		<td style="width:70%; padding-left: 10px; font-weight: bold; font-size: 12px;">
		        		Bollo
	        		</td>
	        		<td style="width:30%; text-align:right; padding-right: 10px; font-weight: bold;">
	        			<?php echo number2string($fisco['costo_bollo']) ?>
	        		</td>
	        	</tr>
			<?php } ?>
	        	
	        </table>
        </td>
    </tr>
<?php } ?>



	<tr><td style="padding: 2px 0; border-bottom: 1px solid #999999;"></td></tr>

	<tr>
        <td style="width:100%;">
	        <table>
	        	<tr>
	        		<td style="width:70%; padding-left: 10px; font-weight: bold;">
		        		TOTALE 
	        		</td>
	        		<td style="width:30%; text-align:right; padding-right: 10px; font-weight: bold; font-size:16px;">
	        			<?php echo $_POST['totale'] ?>
	        		</td>
	        	</tr>
	        </table>
        </td>
    </tr>
    
    <tr><td style="height:30px;"></td></tr>
    
    <tr valign="top">
        <td style="width:100%;">
	        <table>
	        	<tr>
	        		<td style="width:50%; padding-left: 10px; font-weight: bold; font-size:11px;">
		        		<?php	
		        			if(isset($_POST['bollo'])){
		        			
		        				if($fisco['dicitura_bollo'] != ""){
			        				echo $fisco['dicitura_bollo'] ."<br />";
		        				}
			        			
			        			if($_POST['idbollo'] != ""){
				        			echo "ID: " . $_POST['idbollo'];
			        			}
			        			
		        			}
		        		?>
	        		</td>
	        		<td style="width:50%; text-align:right; padding-right: 10px; font-weight: bold; font-size:11px;">
	        			<?php
							if($fisco['iban'] != "" && $fisco['intestazione_cc'] != ""){
								echo "
									Pagamento a mezzo bonifico bancario<br />
									Intestato a: " . $fisco['intestazione_cc'] . "<br />
									IBAN: " . $fisco['iban']  . "
								";
							}
						?>
	        		</td>
	        	</tr>
	        </table>
        </td>
    </tr>
    
    
    <tr><td style="height:30px;"></td></tr>
    
    <tr valign="top">
        <td>
	        <table>
	        	<tr>
	        		<td style="width:100%; padding: 0 10px; font-size:11px;">
		        		<?php echo $layout['note']  ?>
	        		</td>
	        	</tr>
	        </table>
        </td>
    </tr>


    <!--
    <tfoot>
        <tr>
            <th style="font-size: 16px;">
                footer fattura<br />
                footer fattura<br />
                footer fattura<br />
                footer fattura<br />
                footer fattura<br />
                footer fattura<br />
            </th>
        </tr>
    </tfoot>
    -->
</table>

