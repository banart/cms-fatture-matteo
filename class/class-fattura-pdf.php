<?php

	include('connect.php');
	
	$query = " SELECT * FROM fatture ORDER BY id ASC ";
	$result = $conn->query($query);
	$row = $result->fetch_array();

	$srv = implode("|", $_POST['servizi']);
	$prz = implode("|", $_POST['prezzi']);

	if(isset($_POST['bollo'])){
		$bollo = 1;
	}else{
		$bollo = 0;
	}
	
	$idbollo = "";
	if(isset($_POST['bollo']) && isset($_POST['idbollo'])){
		$idbollo = $_POST['idbollo'];
	}
	
	$data_parts = explode("/", $_POST['data']);
	$data = $data_parts[2]."-".$data_parts[1]."-".$data_parts[0];
	$anno = $data_parts[2];
	
	$numero = $_POST['numero'];
	$cliente = $_POST['cliente'];
	$totale_num = $_POST['totale_num'];


	$query = "INSERT INTO fatture (
				numero,
				data,
				anno,
				cliente,
				servizi,
				prezzi,
				totale,
				bollo,
				id_bollo
			) VALUES ('".
				$numero . "','" .
				$data . "','" .
				$anno . "','" .
				$cliente . "','" .
				$srv . "','" .
				$prz . "','" .
				$totale_num . "','" .
				$bollo . "','" .
				$idbollo
			. "')";
			
	if ($conn->query($query) === TRUE) {
		//echo "Inserimento avvenuto con successo!";
		// header("location: ../index.php?page=elenco-fatture");
		//exit();
	} else {
		echo "Errore: " . $query . "<br>" . $conn->error;
	}
	
	
	

	// get the HTML
    ob_start();
    include('class-fattura-dati.php');
    $content = ob_get_clean();

    // convert to PDF
    require_once('../html2pdf/html2pdf.class.php');
    try
    {
        $html2pdf = new HTML2PDF('P', 'A4', 'it');
        $html2pdf->pdf->SetDisplayMode('fullpage');
        $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
        $html2pdf->Output('../pdf/'.$anno.'_'.$numero.'.pdf');
        $html2pdf->Output('../pdf/'.$anno.'_'.$numero.'.pdf', 'F');
    }
    catch(HTML2PDF_exception $e) {
        echo $e;
        exit;
    }
    
    $conn->close();

?>