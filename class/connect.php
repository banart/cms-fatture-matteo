<?php

	include("connection-data.php");

	if(
		!isset($servername) &&
		!isset($username) &&
		!isset($password) &&
		!isset($dbname)
	){
	
		header("location: install");
		
	}else{
	
		$conn = new mysqli($servername, $username, $password, $dbname);
		
		if ($conn->connect_error) {
		   header("location: install");
		}
		
	}

?>