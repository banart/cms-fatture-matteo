<?php
	session_start();

	error_reporting(E_ALL);
	ini_set("display_errors",1);
	
	include('connect.php');

	include('../includes/functions.php');

	//echo "<pre>" . print_r($_FILES,1) . "</pre>";
	
	
	/***************************************/
	/*** ASSEGNO LE VARIABILI PRINCIPALI ***/
	/***************************************/
	
	//Percorso della cartella dove mettere i file caricati dagli utenti
	$uploaddir = '../uploads';
	
	//Recupero il percorso temporaneo del file
	$logofattura_tmp = $_FILES['logofattura']['tmp_name'];
	
	//Recupero il nome originale del file caricato
	$logofattura_name = $_FILES['logofattura']['name'];
	//$logofattura_name = $tstamp . $charshuffled;
	
	//Recupero il tipo di file caricato
	$logofattura_type = $_FILES['logofattura']['type'];
	
	/***************************************/
	
	


	/*******************************************/
	/*** EFFETTUO TUTTI I CONTROLLI SUL FILE ***/
	/*******************************************/
	
	// Verifico che il file sia stato caricato
	if (!isset($_FILES['logofattura']) || !is_uploaded_file($_FILES['logofattura']['tmp_name'])) {
		$_SESSION['logo_error_message'] =  'Non hai inviato nessun file...';
		header("location: ../index.php?page=impostazioni-layout");
		exit;
	}
	
	// Verifico che il file non sia più grande di 1MB
	if ($_FILES['logofattura']['size'] > 1024000) {
		$_SESSION['logo_error_message'] =  'Il file è troppo grande! Puoi caricare file grandi al massimo 1MB!';
		header("location: ../index.php?page=impostazioni-layout");
		exit;
	}
	
	// Verifico che il file sia effettivamente un'immagine
	$is_img = getimagesize($_FILES['logofattura']['tmp_name']);
	if (!$is_img) {
		$_SESSION['logo_error_message'] =  'Puoi inviare solo immagini';
		header("location: ../index.php?page=impostazioni-layout");
		exit;
	}
	
	// Verifico che il file abbia una delle estensioni ammesse
	$ext_ok = array('jpg', 'jpeg', 'png', 'gif');
	$temp = explode('.', $logofattura_name);
	$ext = end($temp);
	if (!in_array($ext, $ext_ok)) {
		$_SESSION['logo_error_message'] =  'Il file ha un estensione non ammessa!';
		header("location: ../index.php?page=impostazioni-layout");
		exit;
	}

	/*******************************************/


	
	//copio il file dalla sua posizione temporanea alla mia cartella upload
	if (move_uploaded_file($logofattura_tmp, $uploaddir . $logofattura_name)) {
	  // echo 'File inviato con successo.';
	}else{
	  echo 'Upload NON valido!'; 
	}
	
	
	
	// Rinomina l'immagine in logo.ext
	$name_array = explode('.', $logofattura_name);
	$ext = end($temp);
	
	$radnomString = randomString();
	$tstamp = time();
	
	$logofattura_oldname = $uploaddir ."/". $uploaddir . $logofattura_name;
	$logofattura_newname = $uploaddir ."/". $radnomString . $tstamp.".".$ext;
	rename($logofattura_oldname, $logofattura_newname);
	
	
	
	// Revupero le informazioni dell'immagine
	list($width, $height, $type, $attr) = getimagesize($logofattura_newname);
	
	
	
	
	// Calcolo le nuove dimensioni assegnando due nuove variabili la dimensione del nuovo file deve essere inferiore a 350 di larghezza e 100 di altezza
	if($height > 100){
		$proporzioni = $width / $height;
		$newheight = 100;
		$newwidth = round($proporzioni * $newheight);
	}else{
		$newheight = $height;
		$proporzioni = $width / $height;
		$newwidth = round($proporzioni * $newheight);
	}
	
	if($newwidth > 350){
		$proporzioni = $height / $width;
		$newwidth = 350;
		$newheight = round($proporzioni * $newwidth);
	}
	



	// Istanzio una variabile per la nuova immagine
	$logotemp = imagecreatetruecolor($newwidth, $newheight);
	imagealphablending($logotemp, false);
	imagesavealpha($logotemp, true);
	
	
	// Verifico il formato dell'immagine caricata e la assegno ad una variabile
	if($ext == "jpg" || $ext == "jpeg"){
		$source = imagecreatefromjpeg($logofattura_newname);
	}elseif($ext == "gif"){
		$source = imagecreatefromgif($logofattura_newname);
	}elseif($ext == "png"){
		$source = imagecreatefrompng($logofattura_newname);
	} 

	// Ridimensiono l'immagine
	imagecopyresampled($logotemp, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
	
	// Creo la nuova immagine png
	imagepng($logotemp, $uploaddir."/logo.png");
	
	// Salvo il nome della nuova immagine nel database (anche se ora non serve più)
	$query = "UPDATE layout SET logo = 'logo.png' WHERE role='admin'";
	if ($conn->query($query) === TRUE) {
		header("location: ../index.php?page=impostazioni-layout");
	} else {
		echo "Errore: " . $query . "<br>" . $conn->error;
	}
	
	
	$conn->close();
	
?>









<?php /*

	// RIDIMENSIONARE IMMAGINE
	
	// Ottengo le informazioni sull'immagine originale
	list($width, $height, $type, $attr) = getimagesize($_SERVER['DOCUMENT_ROOT'].'/foto/mydog.jpg');
	
	// Creo la versione 120*90 dell'immagine (thumbnail)
	$thumb = imagecreatetruecolor(120, 90);
	$source = imagecreatefromjpeg($_SERVER['DOCUMENT_ROOT'].'/foto/mydog.jpg');
	imagecopyresized($thumb, $source, 0, 0, 0, 0, 120, 90, $width, $height);
	
	// Salvo l'immagine ridimensionata
	imagejpeg($thumb, $_SERVER['DOCUMENT_ROOT']."/foto/mydog_thumb.jpg", 75);
	
	
	
	
	// CONVERTIRE IMMAGINE
	
	// Creo l'immagine dal file inizio.png e la salvo in una ariabile
	$im = imagecreatefrompng("inizio.png");
	// Leggo l'immagine dalla variabile e la salvo nel file fine.jpeg
	imagejpeg($im, "fine.jpeg");
	*/
?>
