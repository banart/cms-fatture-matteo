<?php


/*************/
/* ESEMPIO 1 */
/*************/


	// Get real path for our folder
	$rootPath = realpath('folder-to-zip');
	
	// Initialize archive object
	$zip = new ZipArchive();
	$zip->open('file.zip', ZipArchive::CREATE | ZipArchive::OVERWRITE);
	
	// Create recursive directory iterator
	/** @var SplFileInfo[] $files */
	$files = new RecursiveIteratorIterator(
	    new RecursiveDirectoryIterator($rootPath),
	    RecursiveIteratorIterator::LEAVES_ONLY
	);
	
	foreach ($files as $name => $file)
	{
	    // Skip directories (they would be added automatically)
	    if (!$file->isDir())
	    {
	        // Get real and relative path for current file
	        $filePath = $file->getRealPath();
	        $relativePath = substr($filePath, strlen($rootPath) + 1);
	
	        // Add current file to archive
	        $zip->addFile($filePath, $relativePath);
	    }
	}
	
	// Zip archive will be created only after closing object
	$zip->close();
?>








<?php


/*************/
/* ESEMPIO 2 */
/*************/

	

	$zip = new ZipArchive;
	if ($zip->open('test.zip') === TRUE) {
	    $zip->addFile('/path/to/index.txt', 'newname.txt');
	    $zip->close();
	    echo 'ok';
	} else {
	    echo 'failed';
	}
?>





 <?php
 
/*************/
/* ESEMPIO 2 con creazione file */
/*************/
 
    // istanza della classe ZipArchive
    $zip = new ZipArchive();
    // nome del file zip che voglio creare
    $nomeZip = "prova.zip";
    // creo il zip
    if ($zip->open($nomeZip, ZIPARCHIVE::CREATE) !== TRUE) {
    // blocco il codice se la creazione del zip fallisce
    exit("impossibile creare il file zip");
    }
    // aggiungo al file zip il file 'file1.txt'
    $zip->addFile("file1.txt");
    // aggiungo al file zip il file 'file2.txt'
    $zip->addFile("file2.txt");
    // chiudo il file zip e salvo tutte le modifiche fatte ad esso
    $zip->close();
?>

