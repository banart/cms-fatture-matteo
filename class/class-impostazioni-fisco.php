<?php

	// echo "<pre>" . print_r($_POST,1) . "</pre>";

	error_reporting(E_ALL);
	ini_set("display_errors",1);
	
	include('connect.php');
	
	
	
	
	if(isset($_POST['iva'])){
		if($_POST['iva'] == "si"){
			$iva = 1;
			$percent_iva = $conn->real_escape_string($_POST['percent-iva']);
		}else{
			$iva = 0;
			$percent_iva = "";
		}
	}else{
		$iva = NULL;
		$percent_iva = NULL;
	}
	
	if(isset($_POST['ritenuta'])){
		if($_POST['ritenuta'] == "si"){
			$ritenuta = 1;
			$percent_ritenuta = $conn->real_escape_string($_POST['percent-ritenuta']);
		}else{
			$ritenuta = 0;
			$percent_ritenuta = "";
		}
	}else{
		$ritenuta = NULL;
		$percent_ritenuta = NULL;
	}
	
	if(isset($_POST['bollo'])){
		if($_POST['bollo'] == "si"){
			$bollo = 1;
			
			
			// Controllo che il numero sia stato scritto con la virgola, il punto o niente
			if(strpos($_POST['soglia-bollo'], ',') || strpos($_POST['soglia-bollo'], '.')){
				// Se ha la virgola la sostituisco con il punto, altrimenti non esegue nulla
				$soglia_bollo = str_replace(',', '.', $_POST['soglia-bollo']);
				$soglia_bollo = round($soglia_bollo, 2);
				$soglia_bollo = $conn->real_escape_string($soglia_bollo);
			}else{
				$soglia_bollo = $_POST['soglia-bollo'];
			}
			
			// Controllo che il numero sia stato scritto con la virgola, il punto o niente
			if(strpos($_POST['costo-bollo'], ',') || strpos($_POST['costo-bollo'], '.')){
				// Se ha la virgola la sostituisco con il punto, altrimenti non esegue nulla
				$costo_bollo = str_replace(',', '.', $_POST['costo-bollo']);
				$costo_bollo = round($costo_bollo, 2);
				$costo_bollo = $conn->real_escape_string($costo_bollo);
			}else{
				$costo_bollo = $_POST['costo-bollo'];
			}
			
			
			$dicitura_bollo = $conn->real_escape_string($_POST['dicitura-bollo']);
		}else{
			$bollo = 0;
			$soglia_bollo = "";
			$costo_bollo = "";
			$dicitura_bollo = "";
		}
	}else{
		$bollo = NULL;
		$soglia_bollo = NULL;
		$costo_bollo = NULL;
		$dicitura_bollo = NULL;
	}
	


	$intestazione_cc = $conn->real_escape_string($_POST['intestazione-cc']);
	$iban = $conn->real_escape_string($_POST['iban']);
	
	$query = "UPDATE fisco SET
		iva = '".$iva."',
		percent_iva = '".$percent_iva."',
		ritenuta = '".$ritenuta."',
		percent_ritenuta = '".$percent_ritenuta."',
		bollo = '".$bollo."',
		soglia_bollo = '".$soglia_bollo."',
		costo_bollo = '".$costo_bollo."',
		dicitura_bollo = '".$dicitura_bollo."',
		intestazione_cc = '".$intestazione_cc."',
		iban = '".$iban."'
		
		WHERE role='admin'";
	
	
	if ($conn->query($query) === TRUE) {
		//echo "Inserimento avvenuto con successo!";
		header("location: ../index.php?page=impostazioni-fisco");
		exit();
	} else {
		echo "Errore: " . $query . "<br>" . $conn->error;
	}

	$conn->close();
	
?>
