<?php

	include("includes/headinfo.php");

	error_reporting(E_ALL);
	ini_set("display_errors",1);
	
	include("includes/header.php");
	
	if(isset($_SESSION['id'])){
	
		$page="nuova-fattura";
		
		if(isset($_GET['page'])){
			$page = $_GET['page'];
			if(!file_exists ("pages/{$page}.php" )){
				$page = "404";
			}
		}
		
		include("pages/{$page}.php");
		
	}else{
		
		if(isset($_POST['send_reset_password']) || isset($_GET['key'])){
			include("pages/reset-password.php");
		}else{
			include("pages/login.php");
		}
	
	}
	
	include("includes/footer.php");


/*

Manca:
 - Implementare il sistema di password dimenticata
 - Notifica di avvenuta modifica all'interno delle pagine Anagrafica e Fisco
 - Rivedere le icone per Anagrafica Cliente

*/

?>

