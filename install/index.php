<?php
	session_start();
	
	include('../class/connection-data.php');
	
	if(!isset($servername) && !isset($username) && !isset($password) && !isset($dbname)){
		header('location: ../index.php');
	}
	
	if(isset($_SESSION['error_mail_txt'])){
		$error_mail_txt = $_SESSION['error_mail_txt'];
	}else{
		$error_mail_txt = "";
	}
	
	if(isset($_SESSION['error_password_txt'])){
		$error_password_txt = $_SESSION['error_password_txt'];
	}else{
		$error_password_txt = "";
	}
	
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>Invoiz - Install</title>
		

		<link href="../css/bootstrap.min.css" rel="stylesheet">
		<link href="../css/style.css" rel="stylesheet">
		
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<section class="install">
			<div class="container">
			
				<div class="row">
					<div class="col-md-12">
						<h1>
							Invoiz
						</h1>
					</div>
					<div class="col-md-12">
						<p>
							Compila tutti i campi sottostanti per installare Involiz. Conserva le tue credenziali in un posto sicuro, ti serviranno per accedere al sistema.
						</p>
					</div>
				</div>
				
				<form method="post" action="install.php">
					<div class="row">
						<div class="col-md-12">
							<h2>
								Dati database
							</h2>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="form-group">
								<label for="db_host">Host</label>
								<input type="text" name="db_host" id="db_host" class="form-control"/>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="form-group">
								<label for="db_name">Nome DB</label>
								<input type="text" name="db_name" id="db_name" class="form-control"/>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="form-group">
								<label for="db_username">Username DB</label>
								<input type="text" name="db_username" id="db_username" class="form-control"/>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="form-group">
								<label for="db_password">Password DB</label>
								<input type="password" name="db_password" id="db_password" class="form-control"/>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<h2>
								Dati amministratore
							</h2>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="form-group">
								<label for="admin_email1">Email</label>
								<input type="email" name="admin_email1" id="admin_email1" class="form-control"/>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="form-group">
								<label for="admin_email2">Ripeti email</label>
								<input type="email" name="admin_email2" id="admin_email2" class="form-control"/>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="form-group">
								<label for="admin_password1">Password</label>
								<input type="password" name="admin_password1" id="admin_password1" class="form-control"/>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="form-group">
								<label for="admin_password2">Ripeti password</label>
								<input type="password" name="admin_password2" id="admin_password2" class="form-control"/>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<button type="submit" class="btn-install">
								START
							</button>
						</div>
					</div>
				</form>
			</div>
		</section>
	

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="../js/bootstrap.min.js"></script>
	</body>
</html>