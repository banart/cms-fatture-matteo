<?php
	session_start();

	$servername = $_POST['db_host'];
	$username = $_POST['db_username'];
	$password = $_POST['db_password'];
	$dbname = $_POST['db_name'];
	
	$md5pass = md5($_POST['admin_password1']);
	
	
	if($_POST['admin_email1'] != ""){
		if($_POST['admin_email1'] == $_POST['admin_email2']){
			$admin_mail = $_POST['admin_email1'];
		}else{
			$_SESSION['error_mail_txt'] = "Gli indirizzi email non combaciano!";
			header("location: index.php");
		}
	}else{
		$_SESSION['error_mail_txt'] = "Non hai inserito l'indirizzo email!";
		header("location: index.php");
	}
	
	if($_POST['admin_password1'] != ""){
		if($_POST['admin_password1'] == $_POST['admin_password2']){
			$admin_password = $_POST['admin_password1'];
		}else{
			$_SESSION['error_password_txt'] = "Le password non combaciano!";
		}
	}else{
		$_SESSION['error_password_txt'] = "Non hai inserito la password!";
	}
	
	
		
	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
	// Check connection
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	}
	
	
	$errorinstall = 0;
	
	
	
	
	
/*----
--------------
	Creo le tabelle necessarie
--------------
-----*/

	$sql = "
		CREATE TABLE admin (
			id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			email text NOT NULL,
			password text NOT NULL,
			token text NOT NULL
		)ENGINE=InnoDB DEFAULT CHARSET=utf8
	";
		
	if ($conn->query($sql) === TRUE) {
		// echo "OK! Creazione tabella ADMIN riuscita!<br /><br />";
	} else {
		echo "Errore nella creazione delle tabella ADMIN:<br />" . $conn->error . "<br /><br />";
		$errorinstall = 1;
	}
	
	
	$sql = "
		CREATE TABLE anagrafica (
			id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			role varchar(10) NOT NULL,
			ragione_sociale varchar(100) NOT NULL,
			partita_iva int(30) NOT NULL,
			codice_fiscale varchar(30) NOT NULL,
			via varchar(100) NOT NULL,
			civico int(5) NOT NULL,
			cap int(10) NOT NULL,
			citta varchar(50) NOT NULL,
			provincia varchar(50) NOT NULL,
			sito_web varchar(100) NOT NULL,
			email varchar(100) NOT NULL,
			telefono_fisso varchar(30) NOT NULL,
			telefono_mobile varchar(30) NOT NULL,
			fax varchar(30) NOT NULL,
			skype varchar(50) NOT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=utf8
	";
		
	if ($conn->query($sql) === TRUE) {
		// echo "OK! Creazione tabella ANAGRAFICA riuscita!<br /><br />";
	} else {
		echo "Errore nella creazione delle tabella ANAGRAFICA:<br /> " . $conn->error . "<br /><br />";
		$errorinstall = 1;
	}
	
		
	$sql = "
		CREATE TABLE clienti (
			id int(100) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			ragione_sociale varchar(100) DEFAULT NULL,
			partita_iva varchar(20) DEFAULT NULL,
			codice_fiscale varchar(20) DEFAULT NULL,
			via varchar(100) DEFAULT NULL,
			civico int(5) DEFAULT NULL,
			cap int(5) DEFAULT NULL,
			citta varchar(50) DEFAULT NULL,
			provincia varchar(30) DEFAULT NULL,
			sito_web varchar(100) DEFAULT NULL,
			email varchar(100) DEFAULT NULL,
			telefono_fisso varchar(15) DEFAULT NULL,
			telefono_mobile varchar(15) DEFAULT NULL,
			fax varchar(15) DEFAULT NULL,
			skype varchar(100) DEFAULT NULL,
			nome_referente varchar(60) DEFAULT NULL,
			ruolo_referente varchar(100) DEFAULT NULL,
			email_referente varchar(100) DEFAULT NULL,
			telefono_referente varchar(15) DEFAULT NULL,
			skype_referente varchar(100) DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=utf8
	";
		
	if ($conn->query($sql) === TRUE) {
		// echo "OK! Creazione tabella CLIENTI riuscita!<br /><br />";
	} else {
		echo "Errore nella creazione delle tabella CLIENTI:<br />" . $conn->error . "<br /><br />";
		$errorinstall = 1;
	}
	
	
		
	$sql = "
		CREATE TABLE fatture (
			id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			numero int(5) NOT NULL,
			data date NOT NULL,
			anno int(4) NOT NULL,
			cliente int(4) NOT NULL,
			servizi text NOT NULL,
			prezzi text NOT NULL,
			totale double(13,2) NOT NULL,
			bollo tinyint(1) NOT NULL,
			id_bollo varchar(100) NOT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=utf8
	";
		
	if ($conn->query($sql) === TRUE) {
		// echo "OK! Creazione tabella FATTURE riuscita!<br /><br />";
	} else {
		echo "Errore nella creazione delle tabella FATTURE:<br />" . $conn->error . "<br /><br />";
		$errorinstall = 1;
	}
	
	
		
	$sql = "
		CREATE TABLE fisco (
			id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			role varchar(10) NOT NULL,
			iva tinyint(1) NOT NULL,
			percent_iva float NOT NULL,
			ritenuta tinyint(1) NOT NULL,
			percent_ritenuta float NOT NULL,
			bollo tinyint(1) NOT NULL,
			soglia_bollo float NOT NULL,
			costo_bollo float NOT NULL,
			dicitura_bollo text NOT NULL,
			intestazione_cc varchar(100) NOT NULL,
			iban varchar(50) NOT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=utf8
	";
		
	if ($conn->query($sql) === TRUE) {
		// echo "OK! Creazione tabella FISCO riuscita!<br /><br />";
	} else {
		echo "Errore nella creazione delle tabella FISCO:<br />" . $conn->error . "<br /><br />";
		$errorinstall = 1;
	}
	
		
	$sql = "
		CREATE TABLE layout (
			id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			role varchar(30) DEFAULT NULL,
			logo varchar(11) NOT NULL,
			colore varchar(7) NOT NULL,
			note text NOT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=utf8
	";
		
	if ($conn->query($sql) === TRUE) {
		// echo "OK! Creazione tabella LAYOUT riuscita!<br /><br />";
	} else {
		echo "Errore nella creazione delle tabella LAYOUT:<br />" . $conn->error . "<br /><br />";
		$errorinstall = 1;
	}
	
	
		
	$sql = "
		CREATE TABLE province (
			sigla varchar(2) DEFAULT NULL,
			nome varchar(50) DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=utf8
	";
		
	if ($conn->query($sql) === TRUE) {
		// echo "OK! Creazione tabella PROVINCIE riuscita!<br /><br />";
	} else {
		echo "Errore nella creazione delle tabella PROVINCIE:<br />" . $conn->error . "<br /><br />";
		$errorinstall = 1;
	}
	
	
		
	$sql = "
		CREATE TABLE servizi (
			id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			servizio text,
			prezzo text
		) ENGINE=InnoDB DEFAULT CHARSET=utf8
	";
		
	if ($conn->query($sql) === TRUE) {
		// echo "OK! Creazione tabella SERVIZI riuscita!";
	} else {
		echo "Errore nella creazione delle tabella SERVIZI:<br />" . $conn->error;
		$errorinstall = 1;
	}
	
	
	




/*----
--------------
	Inserisco i dati base nelle tabelle dove ne necessitano
--------------
-----*/
	
		
		
	$sql = "
		INSERT INTO admin (
			id,
			email,
			password,
			token
		) VALUES (
			1,
			'" . $_POST['admin_email1'] . "', 
			'" . $md5pass . "',
			'0'
		)";
		
	if ($conn->query($sql) === TRUE) {
		// echo "OK! Inserimento dati in ADMIN!<br /><br />";
	} else {
		echo "Errore nell'inserimento dati in ADMIN:<br />" . $conn->error . "<br /><br />";
		$errorinstall = 1;
	}
	
	
		
	$sql = "
		INSERT INTO anagrafica (
			id,
			role
		) VALUES (
			1,
			'admin'
		)";
		
	if ($conn->query($sql) === TRUE) {
		// echo "OK! Inserimento dati in ANAGRAFICA!<br /><br />";
	} else {
		echo "Errore nell'inserimento dati in ANAGRAFICA:<br />" . $conn->error . "<br /><br />";
		$errorinstall = 1;
	}
	
	
		
	$sql = "
		INSERT INTO fisco (
			id,
			role
		) VALUES (
			1,
			'admin'
		)";
		
	if ($conn->query($sql) === TRUE) {
		// echo "OK! Inserimento dati in FISCO!<br /><br />";
	} else {
		echo "Errore nell'inserimento dati in FISCO:<br />" . $conn->error . "<br /><br />";
		$errorinstall = 1;
	}
	
	
		
	$sql = "
		INSERT INTO layout (
			id,
			role,
			logo,
			colore
		) VALUES (
			1,
			'admin',
			'logo.png',
			'337ab7'
		)";
		
	if ($conn->query($sql) === TRUE) {
		// echo "OK! Inserimento dati in LAYOUT!<br /><br />";
	} else {
		echo "Errore nell'inserimento dati in LAYOUT:<br />" . $conn->error . "<br /><br />";
		$errorinstall = 1;
	}
	
	
		
	$sql = "
		INSERT INTO province (sigla, nome) VALUES
		('ag', 'Agrigento'),
		('al', 'Alessandria'),
		('an', 'Ancona'),
		('ao', 'Aosta'),
		('ar', 'Arezzo'),
		('ap', 'Ascoli Piceno'),
		('at', 'Asti'),
		('av', 'Avellino'),
		('ba', 'Bari'),
		('bt', 'Barletta-Andria-Trani'),
		('bl', 'Belluno'),
		('bn', 'Benevento'),
		('bg', 'Bergamo'),
		('bi', 'Biella'),
		('bo', 'Bologna'),
		('bz', 'Bolzano'),
		('bs', 'Brescia'),
		('br', 'Brindisi'),
		('ca', 'Cagliari'),
		('cl', 'Caltanissetta'),
		('cb', 'Campobasso'),
		('ci', 'Carbonia-iglesias'),
		('ce', 'Caserta'),
		('ct', 'Catania'),
		('cz', 'Catanzaro'),
		('ch', 'Chieti'),
		('co', 'Como'),
		('cs', 'Cosenza'),
		('cr', 'Cremona'),
		('kr', 'Crotone'),
		('cn', 'Cuneo'),
		('en', 'Enna'),
		('fm', 'Fermo'),
		('fe', 'Ferrara'),
		('fi', 'Firenze'),
		('fg', 'Foggia'),
		('fc', 'Forl&igrave;-Cesena'),
		('fr', 'Frosinone'),
		('ge', 'Genova'),
		('go', 'Gorizia'),
		('gr', 'Grosseto'),
		('im', 'Imperia'),
		('is', 'Isernia'),
		('sp', 'La spezia'),
		('aq', 'L''aquila'),
		('lt', 'Latina'),
		('le', 'Lecce'),
		('lc', 'Lecco'),
		('li', 'Livorno'),
		('lo', 'Lodi'),
		('lu', 'Lucca'),
		('mc', 'Macerata'),
		('mn', 'Mantova'),
		('ms', 'Massa-Carrara'),
		('mt', 'Matera'),
		('vs', 'Medio Campidano'),
		('me', 'Messina'),
		('mi', 'Milano'),
		('mo', 'Modena'),
		('mb', 'Monza e della Brianza'),
		('na', 'Napoli'),
		('no', 'Novara'),
		('nu', 'Nuoro'),
		('og', 'Ogliastra'),
		('ot', 'Olbia-Tempio'),
		('or', 'Oristano'),
		('pd', 'Padova'),
		('pa', 'Palermo'),
		('pr', 'Parma'),
		('pv', 'Pavia'),
		('pg', 'Perugia'),
		('pu', 'Pesaro e Urbino'),
		('pe', 'Pescara'),
		('pc', 'Piacenza'),
		('pi', 'Pisa'),
		('pt', 'Pistoia'),
		('pn', 'Pordenone'),
		('pz', 'Potenza'),
		('po', 'Prato'),
		('rg', 'Ragusa'),
		('ra', 'Ravenna'),
		('rc', 'Reggio di Calabria'),
		('re', 'Reggio nell''Emilia'),
		('ri', 'Rieti'),
		('rn', 'Rimini'),
		('rm', 'Roma'),
		('ro', 'Rovigo'),
		('sa', 'Salerno'),
		('ss', 'Sassari'),
		('sv', 'Savona'),
		('si', 'Siena'),
		('sr', 'Siracusa'),
		('so', 'Sondrio'),
		('ta', 'Taranto'),
		('te', 'Teramo'),
		('tr', 'Terni'),
		('to', 'Torino'),
		('tp', 'Trapani'),
		('tn', 'Trento'),
		('tv', 'Treviso'),
		('ts', 'Trieste'),
		('ud', 'Udine'),
		('va', 'Varese'),
		('ve', 'Venezia'),
		('vb', 'Verbano-Cusio-Ossola'),
		('vc', 'Vercelli'),
		('vr', 'Verona'),
		('vv', 'Vibo valentia'),
		('vi', 'Vicenza'),
		('vt', 'Viterbo')";
	
	if ($conn->query($sql) === TRUE) {
		// echo "OK! Inserimento dati in PROVINCIE!<br /><br />";
	} else {
		echo "Errore nell'inserimento dati in PROVINCIE:<br />" . $conn->error . "<br /><br />";
		$errorinstall = 1;
	}



/*----
--------------
	Se non ci sono errori scrivo il file con i dati di connessione
--------------
-----*/


	if ($errorinstall == 0) {

		$conn_file = fopen("../class/connection-data.php", "w") or die("Impossibile aprire il file!");
		
$conn_dbconfig="<?php
	\$servername =\"".$servername."\";
	\$username =\"".$username."\";
	\$password =\"".$password."\";
	\$dbname =\"".$dbname."\";
?>
";

		fwrite($conn_file, $conn_dbconfig);

		fclose($myfile);

	}
	
	
	header("location: ../index.php");
	
	$conn->close();
	
?> 