<div class="wrap">

	<header>
		<nav class="navbar navbar-default">
			<div class="container">
				
				
				<?php if(isset($_SESSION['id'])){ ?>
				
					
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="index.php">
							<span>
								invoiz
							</span>
						</a>
					</div>
	
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				
						<ul class="nav navbar-nav navbar-right">
						
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-file-text"></i><span>Fatture</span><span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="?page=nuova-fattura"><i class="fa fa-plus"></i><span>Nuova fattura</span></a></li>
									<li><a href="?page=elenco-fatture"><i class="fa fa-th-list"></i><span>Elenco fatture</span></a></li>
									<!--<li><a href="?page=servizi"><i class="fa fa-briefcase"></i><span>Servizi</span></a></li>-->
								</ul>
							</li>
							
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-users"></i><span>Clienti</span><span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="?page=nuovo-cliente"><i class="fa fa-plus"></i><span>Nuova cliente</span></a></li>
									<li><a href="?page=elenco-clienti"><i class="fa fa-th-list"></i><span>Elenco clienti</span></a></li>
								</ul>
							</li>
							
							<!-- <li><a href="?page=servizi"><i class="fa fa-briefcase"></i><span>Servizi</span></a></li> -->
							
							<!--
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-briefcase"></i> Servizi</a>
								<ul class="dropdown-menu">
									<li><a href="?page=nuovo-servizio"><i class="fa fa-plus"></i><span>Nuovo servizio</span></a></li>
									<li><a href="?page=elenco-servizi"><i class="fa fa-th-list"></i><span>Elenco servizi</span></a></li>
								</ul>
							</li>
							-->
							
							<!-- <li><a href="?page=impostazioni-anagrafica"><i class="fa fa-cog"></i></a></li> -->
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i><span>Admin</span><span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="?page=impostazioni-anagrafica"><i class="fa fa-user"></i><span>Anagrafica</span></a></li>
									<li><a href="?page=impostazioni-fisco"><i class="fa fa-euro"></i><span>Fisco</span></a></li>
									<li><a href="?page=impostazioni-layout"><i class="fa fa-file-o"></i><span>Layout</span></a></li>
									<li><a href="?page=impostazioni-password"><i class="fa fa-lock"></i><span>Password</span></a></li>
								</ul>
							</li>
							
							<li><a href="class/class-login.php?log=out"><i class="fa fa-sign-out"></i></a></li>
							
						</ul>
					</div><!-- /.navbar-collapse -->
				
				
				<? }else{ ?>
					
					<div class="navbar-header">
						<a class="navbar-brand" href="index.php">
							<span>
								invoiz
							</span>
						</a>
					</div>
					<!--<a class="navbar-login-ico" href="class/class-login.php?log=in"><i class="fa fa-user"></i></a>-->
					
				<?php } ?>
			
			
			</div><!-- /.container-fluid -->
		</nav>
	</header>