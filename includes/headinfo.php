<?php
	session_start();

	include('class/connect.php');
	include('includes/functions.php');
	$date = date("d/m/Y"); 
	$anno = date("Y");
	
	$query = "SELECT * FROM layout WHERE role = 'admin'";
	$result = $conn->query($query);
	$layout = $result->fetch_array();
	
	$base_color = "#".$layout['colore'];
	$rgb_color = hex2rgb($base_color);
	$light_color = rgba2hex("rgba(" . $rgb_color[0] . "," . $rgb_color[1] . "," . $rgb_color[2] . ",0.08)");
	
?>


<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>Invoiz</title>
		

		<link href="css/bootstrap.min.css" rel="stylesheet">
		
		<!-- AGGIUNTO PER MODAL CONFIRM MA FILE GIA' PRESENTE -->
		<link href="css/bootstrap-theme.min.css" rel="stylesheet">
		
		<link href="css/style.css" rel="stylesheet">
		
		<link rel="stylesheet" href="icons/css/font-awesome.min.css">
		<link href='http://fonts.googleapis.com/css?family=Lato:400,300,700' rel='stylesheet' type='text/css'>


		<!-- AGIUNTO PER MODAL CONFIRM - FILE AGGIUNTO (AGGIUNTO ANCHE OMONIMO .LESS) -->
		<link rel="stylesheet" type="text/css" href="css/jquery-confirm.css" />
		
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
		<script src="js/jquery.js"></script>
		<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>-->
		<script src="js/bootstrap.min.js"></script>
		
		<!-- AGIUNTO PER MODAL CONFIRM - FILE AGGIUNTO (AGGIUNTO ANCHE OMONIMO .MIN.JS) -->
        <script src="js/jquery-confirm.js"></script>
        
        <style>
			        
			a{
				color: <?php echo $base_color ?>;
			}
			
			i.sbutton.fa-plus, i.sbutton.fa-pencil{
				background: <?php echo $base_color ?>;
				border-color:  <?php echo $base_color ?>;
			}
			
			i.sbutton.fa-plus:hover, i.sbutton.fa-pencil:hover{
				color: <?php echo $base_color ?>;
			}
			        
        	.navbar-default{
	        	background: <?php echo $base_color ?>;
        	}
        	
        	.navbar-default .navbar-nav > li > a:hover,
        	.navbar-default .navbar-nav > .open > a,
        	.navbar-default .navbar-nav > .open > a:focus,
        	.navbar-default .navbar-nav > .open > a:hover{
	        	color: <?php echo $base_color ?>;
        	}
        	
        	.dropdown-menu > li > a{
	        	color: <?php echo $base_color ?>;
        	}
        	
        	.dropdown-menu{
				border-color:  <?php echo $base_color ?>;
        	}
        	
        	.dropdown-menu > li > a:hover{
	        	background: <?php echo $base_color ?>;
	        	color: #ffffff;
        	}
        	
        	.btn-success, .btn-success:focus{
	        	background: <?php echo $base_color ?>;
	        	border: 1px solid <?php echo $base_color ?>;
        	}
        	
        	.btn-success:hover{
	        	background: #ffffff;
	        	color: <?php echo $base_color ?>;
	        	border: 1px solid <?php echo $base_color ?>;
        	}
        	
        	section:nth-child(2n){
	        	background: <?php echo $light_color ?>;
        	}
        	
        	.riepilogo-fattura section:nth-child(2n){
	        	background: none;
        	}
        	
        	.year-selector, .year-selector:before{
	        	color: <?php echo $base_color ?>;
        	}
        	
        	.lista-anni{
	        	background: <?php echo $base_color ?>;
        	}
        	
        	.lista-anni a:hover{
	        	color:  <?php echo $base_color ?>;
	        	border-color: <?php echo $base_color ?>;
        	}
        	
        	.lista-anni .chiudi-lista-anni i{
	        	color: <?php echo $base_color ?>;
        	}
        	
        	.lista-anni .chiudi-lista-anni:hover i{
	        	background: <?php echo $base_color ?>;
        	}
        	
        	.form-group select option:hover{
	        	background: <?php echo $base_color ?>;
        	}
        	
        	.riepilogo-fattura .dati-cliente{
	        	background: <?php echo $light_color ?>;
        	}
        	
        	.riepilogo-fattura .numero-fattura, .riepilogo-fattura .data-fattura, .riepilogo-fattura .dati-cliente{
	        	border-color: <?php echo $base_color ?>;
        	}
        	
        	.riepilogo-fattura .riepilogo-item-row .row:nth-child(2n){
	        	background: <?php echo $light_color ?>;
        	}
        	
        </style>

	</head>
	<body>