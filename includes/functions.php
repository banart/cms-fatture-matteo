<?php

	//Check and add http://
	function addhttp($url) {
		if ($url != ""){
			if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
		        $url = "http://" . $url;
		    }
		    return $url;
		}
	}
	
	// Genera stringa random di 10 caratteri
	function randomString() {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < 32; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}
	
	
	
	// Trasforma un prezzo espresso in munero in stringa con due cifre decimali
	function number2string($val){
	
		$num = number_format($val, 2,',','');
	
		/*
		if(is_numeric($val)){
			$num = number_format($val, 2,',','.');
		}else{
			$num = "0,00";
		}
		*/

		return $num;
	
		/*
		$val = str_replace('.', ',', $val);
		if(strpos($val, ',')){
			$split_val = explode(",", $val);
			$lunghezza_decimale = strlen($split_val[1]);
			if($lunghezza_decimale == 1){
				$val = $val . "0";
			}
			return $val;
		}else{
			$val = $val . ",00";
			return $val;
		}
		*/
	}
	
	
	
	// Conversione da HEX a RGB
	function hex2rgb($hex){
		$hex = str_replace("#", "", $hex);
		
		if(strlen($hex) == 3){
			$r = hexdec(substr($hex,0,1).substr($hex,0,1));
			$g = hexdec(substr($hex,1,1).substr($hex,1,1));
			$b = hexdec(substr($hex,2,1).substr($hex,2,1));
		}else{
			$r = hexdec(substr($hex,0,2));
			$g = hexdec(substr($hex,2,2));
			$b = hexdec(substr($hex,4,2));
		}
		
		$rgb = array($r,$g,$b);
		
		return $rgb;
		
	}
	
	
	
	// Conversione da RGBA a HEX
	function rgba2hex($string){
		$rgba = array();
		$hex = '';
		$regex = '#\((([^()]+|(?R))*)\)#';
		if (preg_match_all($regex, $string, $matches)){
			$rgba = explode(',', implode(' ', $matches[1]));
		} else {
			$rgba = explode(',', $string);
		}

		if (array_key_exists('3', $rgba)){
			$color = "#";
			for($i=0;$i<3;$i++){
				$total = 255 - $rgba[$i];
				$add = round($total * (1-$rgba['3']));
				$new = $rgba[$i] + $add;
				$ch = dechex($new);
				$color = $color . $ch;
			}
		}
		
		return $color;
	}

	
	
?>