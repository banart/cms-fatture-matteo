
/* --------------- *|
 VALORIZZO VARIABILI
|* --------------- */

var alert_progressivo = "";
var alert_progressivi_mancanti = "";
var alert_confirm = "";
var alert = "";



/* ---------------------- *|
 CREO LA FUNZIONE AL CHANGE
|* ---------------------- */


$(function(){
	$("#form_fattura").on('change',function(event){
	
		/* ------------------------- *|
		 VALORIZZO VARIABILI ON CHANGE
		|* ------------------------- */
		
		// Numero attualmente impostato nel campo progressivo
		var progressivo = $(".numero-progressivo").val();
		
		// Riassegno alla variabile 'date_val' il nuovo valore del campo data
	    date_val = $(".date").val();
	    year_arr = $('.date').val().split("/");
	    year = year_arr[2];


	    // Creo array con i progressivi dell'anno impostato
	    var progressivi_anno = [];
	    $.each(listafatture, function(index,value){
		    annolista = value.split("_");
		    if(annolista[0] == year){
		   		progressivi_anno.push(annolista[1]);
		    }
	    });
	    
	    
	    // CREO UN ARRAY CON I PROGRESSIVI UNICI DELL'ARRAY
	    var progressivi_anno_unici=progressivi_anno.filter(function(itm,i,progressivi_anno){
		   return i==progressivi_anno.indexOf(itm); 
	    });
	    // TROVO IL NUMERO MASSIMO DELL'ARRAY
	    var progressivi_anno_max = Math.max(...progressivi_anno_unici);
	    // VALORIZZO L'ARRAY DEI NUMERI MANCANTI
	    var progressivi_anno_mancanti = [];
	    // CONVERTO L'ARRAY DI STRINGHE-NUMERI IN NUMERI
	    var progressivi_anno_numeri = progressivi_anno_unici.map(Number);
	    // CICLO N VOLTE QUANTO è IL NUMERO MAX
	    for( i = 1 ; i <= progressivi_anno_max ; i++ ){
	    	// SE i NON E' PRESENTE IN progressivi_anno_numeri (quindi da come valore -1)
	    	if(jQuery.inArray(i,progressivi_anno_numeri) == -1){
	    		// AGGIUNGO IL NUMERO ALL'ARRAY DEI NUMERI MANCANTI
		    	progressivi_anno_mancanti.push(i);
	    	}
	    }
	    
	    if(progressivi_anno_max > 0){
		    var prossimo_progressivo = progressivi_anno_max +1;
	    }else{
		    var prossimo_progressivo = 1;
	    }
	    

	    
	    // Assegno valore al cliente
	    var cliente = $("#cliente").val();
	    
	    // Assegno valore primo servizio
	    var servizio = $(".itemRow:first-child input#item").val();


	    // Controllo booleano: Se il progressivo è tra quelli già assegnati
	    bloccante = 0;
	    progressivo_gia_assegnato = 0;
	    progressivo_mancante_corretto = 0;
	    alert_confirm = "";
	    alert_progressivo = "";
	    alert = "";




		/* -------------- *|
		 ESEGUO I CONTROLLI
		|* -------------- */
		
		// Controllo se il nuovo valore non sia tra i progressivi già assegnati
		$.each(listafatture, function(index,value){
			if(value == (year + "_" + progressivo)){
				progressivo_gia_assegnato = 1;
				bloccante = 1;
			}
		});

		// Controllo che il numero valorizzato sia uno dei numeri mancanti. In questo caso cambio il messaggio di alert
		$.each(progressivi_anno_mancanti, function(index, value){
			if(progressivo == value){
				progressivo_mancante_corretto = 1;
			}
		});
		
		// Controllo sul numero progressivo
		if(progressivo_gia_assegnato == 1){
			alert_progressivo = "Il numero progressivo per la fattura è già in uso.<br /><br />";
		}else if(progressivo_mancante_corretto == 1){
			alert_progressivo = "Il numero fattura impostato (<b>" + progressivo + "</b>), è tra i numeri progressivi non ancora assegnati.<br />Considera però che il progressivo successivo, in base alla data inserita, è <b>" + prossimo_progressivo + "</b>.<br /><br />";
		}else{
			// Controllo che il valore del campo progressivo sia quello corretto, cioè il successivo al numero più alto
			if (progressivo != prossimo_progressivo){
				if(progressivo == ""){
					alert_progressivo = "Non hai inserito il numero della fattura<br /><br />";
					bloccante = 1;
				}else{
					alert_progressivo = "Il numero di fattura selezionato non è progressivo.<br />Il numero corretto per l'anno " + year + " è <b>" + prossimo_progressivo + "</b>.<br /><br />";
				}
			}
		}
		
		var separator_prog = "";
		var elenco_progressivi_mancanti = "";
		if(progressivi_anno_mancanti.length){
			$.each(progressivi_anno_mancanti, function(index, value){
				elenco_progressivi_mancanti = elenco_progressivi_mancanti + separator_prog + value;
				separator_prog = " - ";
			});
			alert_progressivi_mancanti = "All'elenco fatture mancano i seguenti progressivi: " + elenco_progressivi_mancanti + "<br /><br />";
		}

		
		
		// Controllo che la data inserita sia uguale alla data di oggi in caso di modifica
		re_date = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
		if(date_val == date){
			alert_date = "";
		}else{
			if(date_val == ""){
				alert_date = "Non hai inserito la data<br /><br />";
				bloccante = 1;
			}else if(!date_val.match(re_date)){
				alert_date = "La data non è scritta correttamente<br /><br />";
				bloccante = 1;
			}else{
				alert_date = "La data inserita non è la data di oggi.<br /><br />";
			}
		}
		
		// Controllo che sia stato scelto il cliente
		if(cliente == ""){
			alert_cliente = "Non hai selezionato il cliente<br /><br />";
			bloccante = 1;
		}else{
			alert_cliente = "";
		}
		
		// Controllo che sia stato inserito un servizio
		if(servizio == ""){
			alert_servizio = "Non hai inserito nessun servizio<br /><br />";
			bloccante = 1;
		}else{
			alert_servizio = "";
		}
		
		
		if(bloccante != 1){
			alert_confirm = "<strong>Si è sicuri di voler procedere?</strong>";
		}else{
			alert_confirm = "";
		}
		
		// Compongo la stringa di alert
		alert  = alert_progressivo + alert_progressivi_mancanti + alert_date + alert_cliente + alert_servizio + alert_confirm;

	});
});



/* --------------------------------------- *|
 CREO IL TRIGGER AL CARICAMENTO DELLA PAGINA
|* --------------------------------------- */

$( window ).load(function(){
	$("#form_fattura").trigger("change");
});






// ALERT DI CONFERMA
$('#form_submit').on('click', function (event) {
	

	if(	alert_progressivo == "" &&
		alert_progressivi_mancanti == "" &&
		alert_date == "" &&
		alert_cliente == "" &&
		alert_servizio == ""
	){
			return;
	}


	event.preventDefault();

	if(bloccante == 1){
		$.alert({
	        theme: 'supervan',
	        title: '<i class="fa fa-warning"></i><br />Attenzione!',
	        content: alert,
	        confirmButton: 'Ho capito',
	        confirm: function(){
	        	// alert('Confirmed!');
	        }
	    });
	}else{
		$.confirm({
	        theme: 'supervan',
	        title: '<i class="fa fa-warning"></i><br />Attenzione!',
	        content: alert,
	        confirm: function(){
	        	$('#form_fattura').submit();
	        },
	        cancel: function(){
	            // alert('Canceled!')
	        }
	    });
	}

    
});
