// Aggiunge (e rimuovere) righe alla pagina "nuova attura".
jQuery(function($) {
    var $button = $('#addRow'),
        $row = $('.itemRow:first'),
        $serviceRow = $('.serviceRow:first');

    $button.click(function() {
    	
    	// Aggiunge voce a Nuova fattura
    	var $newRow = $row.clone();
        $newRow.insertBefore('.endItem');
        $('.itemRow:last input').val('');
        //onChangeNew($newRow);
        
        // Aggiunge riga a Servizi
        $serviceRow.clone().insertBefore('.endItem');
        $('.serviceRow:last input').val('');
        
    });
    
    
    // Rimuove riga a Nuova fattura
	$(document).on('click', '.itemPrice i.fa-close', function() {
	    $(this).parents(".itemRow").remove();
	});
	
	// Rimuove riga a Servizi
	$(document).on('click', '.del-service', function() {
		$(this).parents(".serviceRow").remove();
	});
    
});



// Conferma cancellazione Cliente
$('.del-client').on('click', function (event) {

	event.preventDefault();

	var href = $(this).attr('href');

    $.confirm({
        theme: 'supervan',
        title: '<i class="fa fa-warning"></i><br />Attenzione!',
        content: 'Sei sicuro di voler cancellare questo cliente?',
        confirm: function(){
        	
        	window.location = href;

        },
        cancel: function(){
            // alert('Canceled!')
        }
    });
});



// Conferma cancellazione Fattura
$('.del-fattura').on('click', function (event) {

	event.preventDefault();

	var href = $(this).attr('href');

    $.confirm({
        theme: 'supervan',
        title: '<i class="fa fa-warning"></i><br />Attenzione!',
        content: 'Sei sicuro di voler cancellare questa fattura?',
        confirm: function(){
            window.location = href;
        },
        cancel: function(){
            // alert('Canceled!')
        }
    });
});


/* Conferma cancellazione servizio
$(document).on('click', '.del-service', function() {

	var row = $(this).parents(".serviceRow");

    $.confirm({
        theme: 'supervan',
        title: 'Confirm!',
        content: 'Simple confirm!',
        confirm: function(){
        	row.remove();
        },
        cancel: function(){
	        
        }
    });
});*/




/* VISUALIZZA ANNI FATTURAZIONE */
$('.year-selector').on('click', function () {
	$('.lista-anni').css("top","0");	
})

$('.chiudi-lista-anni').on('click', function(){
	$('.lista-anni').css("top","-100%");	
})



/* Dropdown menu on hover
if ($(window).width() > 768) {
	$(function(){
	$(".dropdown").hover(            
	    function() {
	        $('.dropdown-menu', this).stop( true, true );
	        $(this).toggleClass('new-open');              
	    },
	    function() {
	        $('.dropdown-menu', this).stop( true, true );
	        $(this).toggleClass('new-open');               
	    });
	});
} */


/* Seleziona tutti */
$(document).ready(function() {
    $('#selectall').click(function(event) {
        if(this.checked) {
            $('.selectable').each(function() {
                this.checked = true;             
            });
        }else{
            $('.selectable').each(function() {
                this.checked = false;                   
            });        
        }
    });
    
});



/* Mostra ID bollo se assolto all'origine */
$(document).ready(function() {
	
	var $bollorigine = $('#bollo-origine');
	
    $($bollorigine).click(function(event) {
        if(this.checked) {
            $('.showbollo').show();
        }else{
            $('.showbollo').hide();        
        }
    });
    
    if($bollorigine.prop('checked')) {
        $('.showbollo').show();
    }else{
        $('.showbollo').hide();        
    }
    
});



// Redirect da "crea fattura" all'elenco fatture

$('#submitCreaFattura').click(function(){
	setTimeout(function(){
		window.location.href="index.php?page=elenco-fatture";
	}, 1000)
})



// CONTROLLO SUGLI ELEMENTI DELLA PAGINA FISCO. ABILITA E DISABILITA IN BASE ALLA SELEZIONE DI IVA, RITENUTA E BOLLO

$(document).ready(function() {
	
	
	  /*******/
	 /* IVA */
	/*******/
	
	$('.ck_iva').on('change',function(){
	
		$('#admin-percent-iva').prop('disabled',$(this).val()=='no');
		
		if($(this).val()=='no'){
			$('#admin-percent-iva').css('font-size','0');
		}else{
			$('#admin-percent-iva').css('font-size','inherit');
		}
		
	})
	
	if($('#ivano').prop('checked')){
		$('#admin-percent-iva').css('font-size','0');
	}else{
		$('#admin-percent-iva').css('font-size','inherit');
	}
	
	$('#admin-percent-iva').prop('disabled',$('#ivano').prop('checked'));
	
	
	
	/* PROVA (IN RIFERIMENTO AL BLOCCO DI SOPRA) MA .prop NON PUÒ CONTENERE UN .css VERO?
		$('.ck_iva').on('change',function(){
			$('#admin-percent-iva').prop('disabled',$(this).val()=='no');
			$('#admin-percent-iva').prop($(this).css('font-size','0'),$(this).val()=='no');
		})
		$('#admin-percent-iva').prop('disabled',$('#ivano').prop('checked'));
		$('#admin-percent-iva').prop($(this).css('font-size','0'),$(this).val()=='no');
	*/
	
	
	
	  /************/
	 /* RITENUTA */
	/************/
	
	$('.ck_ritenuta').on('change',function(){
	
		$('#admin-percent-ritenuta').prop('disabled',$(this).val()=='no');
		
		if($(this).val()=='no'){
			$('#admin-percent-ritenuta').css('font-size','0');
		}else{
			$('#admin-percent-ritenuta').css('font-size','inherit');
		}
		
	})
	
	if($('#ritenutano').prop('checked')){
		$('#admin-percent-ritenuta').css('font-size','0');
	}else{
		$('#admin-percent-ritenuta').css('font-size','inherit');
	}
	
	$('#admin-percent-ritenuta').prop('disabled',$('#ritenutano').prop('checked'));
	

	
	  /*********/
	 /* BOLLO */
	/*********/
	
	$('.ck_bollo').on('change',function(){
	
		$('#admin-soglia-bollo').prop('disabled',$(this).val()=='no');
		$('#admin-costo-bollo').prop('disabled',$(this).val()=='no');
		$('#admin-dicitura-bollo').prop('disabled',$(this).val()=='no');
		
		if($(this).val()=='no'){
			$('#admin-soglia-bollo').css('font-size','0');
			$('#admin-costo-bollo').css('font-size','0');
			$('#admin-dicitura-bollo').css('font-size','0');
		}else{
			$('#admin-soglia-bollo').css('font-size','inherit');
			$('#admin-costo-bollo').css('font-size','inherit');
			$('#admin-dicitura-bollo').css('font-size','inherit');
		}
		
	})
	
	if($('#bollono').prop('checked')){
		$('#admin-soglia-bollo').css('font-size','0');
		$('#admin-costo-bollo').css('font-size','0');
		$('#admin-dicitura-bollo').css('font-size','0');
	}else{
		$('#admin-soglia-bollo').css('font-size','inherit');
		$('#admin-costo-bollo').css('font-size','inherit');
		$('#admin-dicitura-bollo').css('font-size','inherit');
	}
	
	$('#admin-soglia-bollo').prop('disabled',$('#bollono').prop('checked'));
	$('#admin-costo-bollo').prop('disabled',$('#bollono').prop('checked'));
	$('#admin-dicitura-bollo').prop('disabled',$('#bollono').prop('checked'));

});













